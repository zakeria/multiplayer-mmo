package org.mobmmo.screen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.mobmmo.GameClient;
import org.mobmmo.entity.Entity;
import org.mobmmo.entity.Player;
import org.mobmmo.util.TextFile;
import org.mobmmo.util.TextFileLoader;
import org.mobmmo.world.World;
import org.mobmmo.world.map.MapConstants;
import org.mobmmo.world.map.MapData;
import org.mobmmo.world.map.TileConfig;
import org.mobmmo.world.map.TileMap;
import org.mobmmo.world.map.TileSet;
import org.mobmmo.world.map.io.MapReader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

/**
 * @author Zakeria
 *
 */
public final class GameScreen implements Screen, InputProcessor {
	/**
	 * s The {@link GameClient}.
	 */
	private final GameClient client;
	/**
	 * The game {@link World}.
	 */
	private final World world;
	/**
	 * A {@link SpriteBatch} used for {@link BitmapFont}s.
	 */
	private SpriteBatch spriteBatch;
	/**
	 * A {@link SpriteBatch} used for map related textures.
	 */
	private SpriteBatch mapBatch;
	/**
	 * Used to render text using a {@link SpriteBatch}.
	 */
	private BitmapFont font;
	/**
	 * The {@link OrthographicCamera} camera.
	 */
	private OrthographicCamera camera;
	/**
	 * The {@link AssetManager}.
	 */
	private AssetManager assetManager;

	/**
	 * Tile transformations.
	 */
	private Vector3 touch;
	private Matrix4 isoTransform;
	private Matrix4 invIsotransform;
	private Matrix4 id;

	/**
	 * Cached {@link TileMap}s.
	 */
	private final Map<Integer, TileMap> maps = new HashMap<>();
	/**
	 * A list of loaded {@link TileSet}s.
	 */
	private final List<TileSet> tileSets = new ArrayList<>();

	/**
	 * The current map being rendered.
	 */
	private TileMap currentMap = null;

	/**
	 * The selected tile index.
	 */
	private int selectedX = 0, selectedY = 0;

	private boolean loadingRegion;

	public GameScreen(GameClient client) {
		this.client = client;
		this.world = new World(this);
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	public void create() {
		Gdx.input.setInputProcessor(new InputMultiplexer(this));
		camera = new OrthographicCamera();

		assetManager = new AssetManager();

		Gdx.app.log(getClass().getName(), "Loading assets...");

		assetManager.load("characters/player.png", Texture.class);
		assetManager.load("tilesets/tileset_0.png", Texture.class);
		assetManager.load("tilesets/tileset_1.png", Texture.class);
		assetManager.load("tilesets/tileset_2.png", Texture.class);

		assetManager.setLoader(TextFile.class, new TextFileLoader(new InternalFileHandleResolver()));
		assetManager.load("maps/0.txt", TextFile.class);

		assetManager.finishLoading();

		// tile set partitioning.
		for (int tilesetId = 0; tilesetId < MapConstants.TILESET_COUNT; tilesetId++) {
			Texture tilesetTexture = assetManager.get("tilesets/tileset_" + tilesetId + ".png", Texture.class);
			// the amount of regions in this tile set texture.
			int length = (tilesetTexture.getWidth() / MapConstants.TILE_WIDTH) * tilesetTexture.getHeight()
					/ MapConstants.TILE_HEIGHT;
			// the texture regions
			TextureRegion[] tileSetData = new TextureRegion[length];
			// the id of the texture region.
			int textureRegionId = 0;
			// set the region boundaries.
			for (int depth = 0; depth < tilesetTexture.getHeight() / MapConstants.TILE_HEIGHT; depth++) {
				for (int x = 0; x < tilesetTexture.getWidth() / MapConstants.TILE_WIDTH; x++) {
					tileSetData[textureRegionId] = new TextureRegion(tilesetTexture, x * MapConstants.TILE_WIDTH,
							depth * MapConstants.TILE_HEIGHT, MapConstants.TILE_WIDTH, MapConstants.TILE_HEIGHT);
					textureRegionId++;
				}
			}
			// add the new tile set to the ArrayList.
			TileSet tileSet = new TileSet(tilesetId, tileSetData);
			tileSets.add(tilesetId, tileSet);
		}
		Gdx.app.log(getClass().getName(), "Constructing tile maps...");

		for (int i = 0; i < MapConstants.MAP_COUNT; i++) {
			TileMap tileMap = MapReader
					.constructTileMap(assetManager.get("maps/" + i + ".txt", TextFile.class).getByteArray(), this);
			maps.put(tileMap.getId(), tileMap);
		}
		Gdx.app.log(getClass().getName(), "Loaded " + maps.size() + " map(s).");

		//opengl texture blending
		GL20 gl = Gdx.graphics.getGL20();
    	gl.glEnable(GL20.GL_BLEND);
    	gl.glEnable(GL20.GL_TEXTURE_2D);
    	gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		spriteBatch = new SpriteBatch();
		mapBatch = new SpriteBatch();

		id = new Matrix4();
		id.idt();

		// create the isometric transform
		isoTransform = new Matrix4();
		isoTransform.idt();
		isoTransform.translate(0.0f, 0.25f, 0.0f);
		isoTransform.scale((float) (MapConstants.TILE_WIDTH / Math.sqrt(2.0)),
				(float) (MapConstants.TILE_HEIGHT / Math.sqrt(2.0)), 1.0f);
		isoTransform.rotate(0.0f, 0.0f, 1.0f, -45.0f);

		// ... and the inverse matrix
		invIsotransform = new Matrix4(isoTransform);
		invIsotransform.inv();

		touch = new Vector3();

		font = new BitmapFont();
		font.setColor(Color.WHITE);

		setCurrentMap(maps.get(0));

		Sprite sprite = new Sprite(assetManager.get("characters/player.png", Texture.class));

		Player player = new Player(client.getId(), sprite, world);
		world.addEntity(player);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		mapBatch.setProjectionMatrix(camera.combined);
		mapBatch.setTransformMatrix(id);
		mapBatch.begin();
		// render the map as isometric
		if (currentMap != null) {
			for (int x = 0; x < currentMap.getWidth(); x++) {
				for (int y = currentMap.getHeight() - 1; y >= 0; y--) {
					int worldX = (x * MapConstants.TILE_WIDTH / 2) + (y * MapConstants.TILE_WIDTH / 2);
					int worldY = -(x * MapConstants.TILE_HEIGHT / 2) + (y * MapConstants.TILE_HEIGHT / 2);

					// get the map data for the current map.
					MapData mapData = currentMap.getData();
					// get the tile configuration for the current tile.
					TileConfig tileConfig = mapData.getTiles()[x][y].getTileConfig();

					// the tile set this tile belongs to.
					TileSet tileSet = mapData.getTilesets()[tileConfig.getTilesetId()];

					// the texture id in a tile set that this tile's texture
					// belongs to.
					int textureId = tileConfig.getTextureId();

					if (x == selectedX && y == selectedY) {
					} else {
						//would probably be a good idea to render only the tiles that are in the view frustum.
						mapBatch.draw(tileSet.getTileSet()[textureId], worldX, worldY, MapConstants.TILE_WIDTH, MapConstants.TILE_HEIGHT);
					}
				}
			}
			// update entities stored in the World.
			final List<Entity> entities = new ArrayList<>(world.getEntities());
			for (int i = 0; i < entities.size(); i++) {
				Entity entity = entities.get(i);
				if (entity == null) {
					continue;
				}
				if (entity.getStepManager().getSteps().size() > 0) {
					entity.getStepManager().update(delta);
				}
				entity.getSprite().draw(mapBatch);
			}
		}

		mapBatch.end();
		mapBatch.setTransformMatrix(isoTransform);

		spriteBatch.begin();
		font.draw(spriteBatch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 10, Gdx.graphics.getHeight());
		font.draw(spriteBatch, "Local entities: " + (world.getEntities().size() - 1), 10,
				Gdx.graphics.getHeight() - 20);
		if (isLoadingRegion())
			font.draw(spriteBatch, "Loading region...", 10, 40);

		spriteBatch.end();

		camera.position.set(world.getPlayer().getSprite().getX(), world.getPlayer().getSprite().getY(), 0);

		camera.update();
	}

	@Override
	public void resize(final int width, final int height) {
		Gdx.app.log(getClass().getName(), "Screen resized to " + width + " " + height);
		camera = new OrthographicCamera(width, height);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		touch.set(screenX, screenY, 0);
		camera.unproject(touch);
		touch.mul(invIsotransform);

		if (currentMap == null)
			return false;

		// prevent clicking out of map boundary.
		if ((int) touch.x < 0 || (int) touch.y < 0 || (int) touch.x > currentMap.getWidth() - 1
				|| (int) touch.y > currentMap.getHeight() - 1) {
			return false;
		}
		selectedX = (int) touch.x;
		selectedY = (int) touch.y;

		// its not a walkable tile.
		TileConfig config = currentMap.getData().getTiles()[selectedX][selectedY].getTileConfig();
		if (!(config.isWalkable())) {
			return false;
		}
		// send a walk request for the selected tile.
		client.requestWalk(selectedX, selectedY);
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		if (Gdx.input.isKeyPressed(Input.Keys.R)) {
			camera.rotate(-0.3f, 0, 0, 1);
		} else if (Gdx.input.isKeyPressed(Input.Keys.F)) {
			camera.rotate(0.3f, 0, 0, 1);
		} else if (Gdx.input.isKeyPressed(Input.Keys.I)) {
			camera.zoom -= 0.12;
		} else if (Gdx.input.isKeyPressed(Input.Keys.O)) {
			camera.zoom += 0.12;
		}
		return false;
	}

	public GameClient getClient() {
		return client;
	}

	public OrthographicCamera getCamera() {
		return camera;
	}

	public World getWorld() {
		return world;
	}

	/**
	 * Gets the {@link AssetManager}.
	 * 
	 * @return the assetManager
	 */
	public AssetManager getAssetManager() {
		return assetManager;
	}

	/**
	 * Gets the cached {@link TiledMap}s.
	 * 
	 * @return the maps
	 */
	public Map<Integer, TileMap> getMaps() {
		return maps;
	}

	/**
	 * Gets the selected tile x index.
	 * 
	 * @return selectX the selected tile x
	 */
	public int selectedX() {
		return selectedX;
	}

	/**
	 * Gets the selected tile y index.
	 * 
	 * @return selectY the selected tile y
	 */
	public int selectedY() {
		return selectedY;
	}

	/**
	 * Sets the current {@link TileMap}.
	 * 
	 * @param the
	 *            currentMap
	 */
	public void setCurrentMap(TileMap currentMap) {
		this.currentMap = currentMap;
	}

	/**
	 * Gets the available tile sets.
	 * 
	 * @return the tileSets
	 */
	public List<TileSet> getTileSets() {
		return tileSets;
	}

	public boolean isLoadingRegion() {
		return loadingRegion;
	}

	public void setLoadingRegion(boolean loadingRegion) {
		this.loadingRegion = loadingRegion;
	}
}
