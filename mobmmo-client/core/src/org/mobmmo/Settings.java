package org.mobmmo;

import java.security.Key;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;

/**
 * @author Zakeria
 *
 */
public final class Settings {
	/**
	 * The {@link KeyFactory}.
	 */
	private final KeyFactory factory;
	/**
	 * The public RSA key.
	 */
	private final RSAPublicKey publicKey;
	/**
	 * The AES key belonging to a session.
	 */
	private final Key aesKey;

	/**
	 * Initialises the game client.
	 */
	public Settings(KeyFactory factory, Key aesKey, RSAPublicKey publicKey) {
		this.factory = factory;
		this.aesKey = aesKey;
		this.publicKey = publicKey;
	}
	/**
	 * Get the AES Key.
	 * @return aesKey
	 */
	public Key getAesKey() {
		return aesKey;
	}
	/**
	 * Get the key factory.
	 * @return factory
	 */
	public KeyFactory getKeyFactory() {
		return factory;
	}
	/**
	 * Get the public key.
	 * @return publicKey
	 */
	public RSAPublicKey getPublicKey() {
		return publicKey;
	}
}
