package org.mobmmo;

import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.logging.Logger;

import org.mobmmo.net.Connection;
import org.mobmmo.net.packet.Packet;
import org.mobmmo.net.packet.PacketManager;
import org.mobmmo.net.security.KeyGenerator;
import org.mobmmo.net.security.SecurityConstants;
import org.mobmmo.screen.GameScreen;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;

/**
 * @author Zakeria
 */
public class GameClient extends ApplicationAdapter implements Runnable {

	private static final Logger logger = Logger.getLogger(GameClient.class.getName());

	private final GameScreen gameScreen;

	private final Connection connection;

	private Settings settings;

	// test position used for unit tests.
	private int testX, testY;

	/**
	 * The id of the client.
	 */
	private int id;

	private final PacketManager packetManager;

	private boolean loggedIn;

	public GameClient() {
		connection = new Connection(this);
		gameScreen = new GameScreen(this);
		packetManager = new PacketManager(this);
		packetManager.loadConfiguration();
		new Thread(this).start();
	}

	/**
	 * Initialises the game client.
	 */
	private void initialise() {
		try {
			KeyFactory factory = KeyFactory.getInstance("RSA");
			BigInteger modulus = new BigInteger(SecurityConstants.RSA_PUBLIC_MODULES);
			BigInteger exponent = new BigInteger(SecurityConstants.RSA_PUBLIC_EXPONENT);
			RSAPublicKeySpec spec = new RSAPublicKeySpec(modulus, exponent);
			RSAPublicKey publicKey = (RSAPublicKey) factory.generatePublic(spec);
			Key aesKey = KeyGenerator.generateAESKey();
			settings = new Settings(factory, aesKey, publicKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void create() {
		System.err.println("yo from other side");
		if (!connection.isConnected()) {
			logger.severe("Not logged in to the server, aborting...");
			System.exit(0);
		}
		// exchange the AES key.
		connection.exchangeKey();
		// send the login request.
		login();
		// wait for the client to log in.
		while (!loggedIn) {
			logger.info("Logging in ...");
		}
		// boolean flag = true? for all gamescreen render etc.//leave create
		// since its needed and is done once.
		gameScreen.create();
	}

	@Override
	public void resize(final int width, final int height) {
		gameScreen.resize(width, height);
	}

	@Override
	public void render() {
		gameScreen.render(Gdx.graphics.getDeltaTime());
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	/**
	 * Sends the login packet.
	 */
	public void login() {
		// random name values for testing.
		int random = (int) (Math.random() * 929 + 1);
		String name = "test" + random;

		Packet packet = new Packet(1).allocate(64);
		packet.putString(name);
		packet.putString("testPass");
		packet.getBuffer().flip();
		packetManager.addOutGoing(packet);
	}

	/**
	 * Sends a walking request,
	 * 
	 * @param x
	 * @param y
	 */
	public void requestWalk(int x, int y) {
		Packet packet = new Packet(2).allocate(2);
		packet.putByte(x);
		packet.putByte(y);
		packet.getBuffer().flip();
		packetManager.addOutGoing(packet);
	}

	/**
	 * Sends the position.
	 * 
	 * @param x
	 * @param y
	 */
	public void sendPosition(int x, int y) {
		Packet packet = new Packet(3).allocate(2);
		packet.putByte(x);
		packet.putByte(y);
		packet.getBuffer().flip();
		packetManager.addOutGoing(packet);
	}

	/**
	 * Handles a login response.
	 * 
	 * @param response
	 *            the response code
	 */
	public void handleResponse(int response) {
		if (response == 1)
			System.out.println("success");
		else if (response == 2)
			System.out.println("already online");
		else if (response == 3)
			System.out.println("server is full");
	}

	public GameScreen getGameScreen() {
		return gameScreen;
	}

	public Connection getConnection() {
		return connection;
	}

	public PacketManager getPacketManager() {
		return packetManager;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTestX() {
		return testX;
	}

	public int getTestY() {
		return testY;
	}

	public void setTestX(int testX) {
		this.testX = testX;
	}

	public void setTestY(int testY) {
		this.testY = testY;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public Settings getSettings() {
		return settings;
	}

	@Override
	public void run() {
		connection.connect();
		new Thread(connection).start();
		initialise();
		connection.setReadReady(true);
	}
}