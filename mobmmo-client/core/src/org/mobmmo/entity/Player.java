package org.mobmmo.entity;

import org.mobmmo.world.World;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Represents this player.
 * 
 * @author Zakeria
 *
 */
public class Player extends Entity {

	public Player(int id, Sprite sprite, World world) {
		super(id, sprite, world);
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
	}
}