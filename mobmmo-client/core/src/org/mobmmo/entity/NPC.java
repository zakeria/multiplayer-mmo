package org.mobmmo.entity;

import org.mobmmo.world.World;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class NPC extends Entity {

	public NPC(int id, Sprite sprite, World world) {
		super(id, sprite, world);
	}

	@Override
	public void setName(String name) {}

	@Override
	public boolean equals(Object obj) {
		return false;
	}

}
