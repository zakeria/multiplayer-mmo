package org.mobmmo.entity;

import org.mobmmo.world.World;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * @author Zakeria
 * 
 */
public abstract class Entity {
	/**
	 * The session id of this entity.
	 */
	private final int id;
	/**
	 * The name of this Entity.
	 */
	protected String name;

	/**
	 * The world this entity is in.
	 */
	protected final World world;

	private final EntityStepManager stepManager;

	private final Sprite sprite;

	public Entity(int id, Sprite sprite, World world) {
		this.id = id;
		this.sprite = sprite;
		this.world = world;
		this.stepManager = new EntityStepManager(this);
	}

	/**
	 * Get the sprite belonging to this entity.
	 * 
	 * @return sprite
	 */
	public Sprite getSprite() {
		return sprite;
	}

	/**
	 * Get the name of the entity.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	public EntityStepManager getStepManager() {
		return stepManager;
	}

	/**
	 * Get the id of the entity.
	 * 
	 * @return id
	 */
	public int getId() {
		return id;
	}

	public abstract void setName(String name);

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Entity) {
			Entity entity = (Entity) obj;
			if (id == entity.getId()) {
				return true;
			}
		}
		return false;
	}

	public World getWorld() {
		return world;
	}
}
