package org.mobmmo.entity;

import org.mobmmo.world.World;

import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Represents a local {@link Entity}.
 * 
 * @author Zakeria
 *
 */
public final class LocalPlayer extends Entity {
	
	public LocalPlayer(int id, Sprite sprite, World world) {
		super(id, sprite, world);
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
}
