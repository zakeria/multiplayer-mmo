package org.mobmmo.entity;

import java.util.ArrayList;
import java.util.List;

import org.mobmmo.GameClient;
import org.mobmmo.util.Misc;
import org.mobmmo.world.World;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * @author Zakeria
 *
 */
public final class EntityStepManager {
	/**
	 * The {@link Entity} this step manager belongs to.
	 */
	private final Entity entity;
	/**
	 * The velocity of the current {@link EntityStep}.
	 */
	private Vector2 velocity = new Vector2();
	/**
	 * The list of {@link EntityStep}s that form a path.
	 */
	private List<EntityStep> steps = new ArrayList<>();
	/**
	 * The index of the current {@link EntityStep}.
	 */
	private int index = 0;
	/**
	 * An instance of the {@link World}.
	 */
	private final World world;
	/**
	 * The {@link GameClient}.
	 */
	private final GameClient client;

	public EntityStepManager(Entity entity) {
		this.entity = entity;
		this.world = entity.getWorld();
		this.client = world.getScreen().getClient();
	}

	/**
	 * Adds a new {@link EntityStep} to the path.
	 * 
	 * @param x
	 *            the x position.
	 * @param y
	 *            the y position.
	 * @param speed
	 *            the speed of the step, the server sends 30 when walking, 60
	 *            when running.
	 */
	public void addStep(int x, int y, int speed) {
		Vector2 worldPos = Misc.tileToWorld(x, y);
		EntityStep step = new EntityStep(worldPos.x, worldPos.y, x, y, speed);
		steps.add(step);
	}

	/**
	 * Update the movement for the {@link Entity}.
	 * 
	 * @param delta
	 *            update the movement based on delta time.
	 */
	public void update(float delta) {
		EntityStep step = steps.get(index);
		float x = entity.getSprite().getX();
		float y = entity.getSprite().getY();
		float angle = (float) Math.atan2(steps.get(index).getY() - y, steps.get(index).getX() - x);
		velocity.set((float) Math.cos(angle) * step.getSpeed(), (float) Math.sin(angle) * step.getSpeed());

		entity.getSprite().setPosition(x + velocity.x * delta, y + velocity.y * delta);
		entity.getSprite().setRotation(angle * MathUtils.radiansToDegrees);

		if (reached()) {
			entity.getSprite().setPosition(steps.get(index).getX(), steps.get(index).getY());
			if (index + 1 >= steps.size()) {
				index = 0;
				steps.clear();
				entity.getSprite().setRotation(0);
				if (entity instanceof Player)
					client.sendPosition(step.getTileX(), step.getTileY());
			} else {
				if (entity instanceof Player)
					client.sendPosition(step.getTileX(), step.getTileY());
				index++;
			}
		}
	}

	/**
	 * A flag indicating when a {@link EntityStep} has been reached.
	 * 
	 * @return true when reached, otherwise false.
	 */
	private boolean reached() {
		float x = entity.getSprite().getX();
		float y = entity.getSprite().getY();
		EntityStep step = steps.get(index);
		return Math.abs(steps.get(index).getX() - x) <= step.getSpeed() * Gdx.graphics.getDeltaTime()
				&& Math.abs(steps.get(index).getY() - y) <= step.getSpeed() * Gdx.graphics.getDeltaTime();
	}

	/**
	 * Clears the steps in the queue.
	 */
	public void clearSteps() {
		steps.clear();
		index = 0;
	}

	/**
	 * Get the current path.
	 * 
	 * @return steps
	 */
	public List<EntityStep> getSteps() {
		return steps;
	}
}
