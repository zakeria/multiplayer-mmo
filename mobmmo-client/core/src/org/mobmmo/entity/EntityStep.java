package org.mobmmo.entity;

/**
 * @author Zakeria
 *
 */
public final class EntityStep {

	private final float x;

	private final float y;

	private final int tileX;

	private final int tileY;

	/**
	 * The speed of this step.
	 */
	private final int speed;

	public EntityStep(float x, float y, int tileX, int tileY, int speed) {
		this.x = x;
		this.y = y;
		this.tileX = tileX;
		this.tileY = tileY;
		this.speed = speed;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public int getTileX() {
		return tileX;
	}

	public int getTileY() {
		return tileY;
	}

	/**
	 * The speed of the step.
	 * 
	 * @return speed
	 */
	public int getSpeed() {
		return speed;
	}
}
