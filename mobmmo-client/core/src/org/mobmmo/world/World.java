package org.mobmmo.world;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.mobmmo.GameClient;
import org.mobmmo.entity.Entity;
import org.mobmmo.entity.NPC;
import org.mobmmo.entity.Player;
import org.mobmmo.screen.GameScreen;

public final class World {
	/**
	 * A list of local {@link Entity}s.
	 */
	private final List<Entity> entities = new ArrayList<>(1000);
	/**
	 * The {@link GameScreen}.
	 */
	private final GameScreen screen;
	/**
	 * This client's {@link Player}.
	 */
	private Player player;

	public World(GameScreen screen) {
		this.screen = screen;
	}

	/**
	 * Adds an Entity to this world.
	 * 
	 * @param entity
	 */
	public synchronized void addEntity(Entity entity) {
		if (entities.contains(entity))
			return;
		if (entity instanceof Player) {
			player = (Player) entity;
		}
		entities.add(entity);
	}

	/**
	 * Removes an Entity from this world.
	 * 
	 * @param entity
	 */
	public synchronized void removeEntity(Entity entity) {
		if (!entities.contains(entity))
			return;
		entities.remove(entity);
	}

	/**
	 * Clears the local {@link Player}s stored in the {@link Entity} list.
	 */
	public synchronized void clearLocalPlayers() {
		for (Iterator<Entity> it = entities.iterator(); it.hasNext();) {
			Entity entity = it.next();
			if (entity instanceof NPC) {
				continue;
			} else if (entity instanceof Player) {
				continue;
			} else {
				it.remove();
			}
		}
	}

	/**
	 * Gets a local player by its id.
	 * 
	 * @param id
	 * @return
	 */
	public Entity getEntity(int id) {
		Entity entity = null;
		final List<Entity> entities = new ArrayList<>(this.entities);
		for (Entity e : entities) {
			if(id == player.getId()) {
				return player;
			}
			if (e.getId() == id) {
				return e;
			}
		}
		return entity;
	}

	/**
	 * Get the local {@link Entity} list.
	 * 
	 * @return
	 */
	public List<Entity> getEntities() {
		return entities;
	}

	/**
	 * Get the {@link GameScreen}.
	 * 
	 * @return screen
	 */
	public GameScreen getScreen() {
		return screen;
	}
	/**
	 * Get the {@link GameClient}'s {@link Player}.
	 * @return player
	 */
	public Player getPlayer() {
		return player;
	}
}