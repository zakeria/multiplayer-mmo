package org.mobmmo.world.map;

/**
 * Stores {@link TileMap} data.
 * 
 * @author Zakeria
 *
 */
public final class MapData {

	/**
	 * The tiles belonging to a {@link TileMap}.
	 */
	private final Tile[][] tiles;

	/**
	 * The tile sets belonging to a {@link TileMap}.
	 */
	private final TileSet[] tileSets;

	public MapData(Tile[][] tiles, TileSet[] tileSets) {
		this.tiles = tiles;
		this.tileSets = tileSets;
	}

	/**
	 * Gets the tile data.
	 * 
	 * @return the tiles
	 */
	public Tile[][] getTiles() {
		return tiles;
	}

	/**
	 * Getse the {@link TileSet}s.
	 * 
	 * @return the tileSets
	 */
	public TileSet[] getTilesets() {
		return tileSets;
	}
}
