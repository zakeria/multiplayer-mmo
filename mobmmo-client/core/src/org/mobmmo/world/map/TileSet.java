package org.mobmmo.world.map;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * @author Zakeria
 *
 */
public final class TileSet {
	/**
	 * The {@link TextureRegion}s of a tile set.
	 */
	private final TextureRegion[] tileSet;

	/**
	 * The tile set id.
	 */
	private final int id;

	public TileSet(int id, TextureRegion[] tileSet) {
		this.id = id;
		this.tileSet = tileSet;
	}

	/**
	 * Gets the tile set id.
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the tile set.
	 * @return the tileSet
	 */
	public TextureRegion[] getTileSet() {
		return tileSet;
	}
}