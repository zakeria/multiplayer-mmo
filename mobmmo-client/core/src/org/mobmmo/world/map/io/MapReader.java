package org.mobmmo.world.map.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;


import org.mobmmo.screen.GameScreen;
import org.mobmmo.world.map.MapData;
import org.mobmmo.world.map.Tile;
import org.mobmmo.world.map.TileConfig;
import org.mobmmo.world.map.TileMap;
import org.mobmmo.world.map.TileSet;

/**
 * Constructs a {@link TileMap} from a {@link File}.
 * @author Zakeria
 *
 */
public class MapReader {

	/**
	 * Convert the byte array into a temp file.
	 * @param data
	 * @return
	 */
	private static File byteArrayToFile(byte[] data) {
		File tempFile = null;
		FileOutputStream os = null;
		try {
			tempFile = File.createTempFile("map", "");
			os = new FileOutputStream(tempFile);
			os.write(data);
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tempFile;	
	}

	
	/**
	 * Constructs a {@link TileMap} from a {@link File}.
	 * 
	 * @param file
	 *            the file
	 * @return the constructed {@link TileMap}.
	 */
	public static TileMap constructTileMap(byte[] data, GameScreen screen) {
		MapData mapData = null;
		int mapId = -1;
		int width = -1;
		int height = -1;
		try {
			File mapFile = byteArrayToFile(data);
			
			FileReader reader = new FileReader(mapFile);
			BufferedReader br = new BufferedReader(reader);
			
			//read the map id, and width (32wx32h default).
			String[] mapInfo = br.readLine().split(":");
			mapId = Integer.parseInt(mapInfo[0]);
			width = Integer.parseInt(mapInfo[1]);
			height = Integer.parseInt(mapInfo[2]);
		
			//read the tile sets used for a map.
			String[] tileSetInfo = br.readLine().split(":");
			TileSet[] tileSets = new TileSet[tileSetInfo.length];
			for (int i = 0; i < tileSetInfo.length; i++) {
				int tileSetId = Integer.parseInt(tileSetInfo[i]);
				tileSets[i] = screen.getTileSets().get(tileSetId);				
			}
			//read the lines containing tile information.
			Tile[][] tiles = new Tile[width][height];
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] inputString = line.split(" ");
				for (int i = 0; i < inputString.length; i++) {
					String[] tileString = inputString[i].split(":");
					int tileX = Integer.parseInt(tileString[0]);
					int tileY = Integer.parseInt(tileString[1]);
					int tilesetId = Integer.parseInt(tileString[2]);
					int textureId = Integer.parseInt(tileString[3]);
					boolean walkable = (Integer.parseInt(tileString[4])) == 1 ? true : false;
					 
				    TileConfig tileConfig = new TileConfig(tilesetId, textureId, walkable);
					tiles[tileX][tileY] = new Tile(tileX, tileY, tileConfig);
					mapData = new MapData(tiles, tileSets);
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new  TileMap(mapId, width, height, mapData);
	}
}
