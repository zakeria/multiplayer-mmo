package org.mobmmo.world.map;

/**
 * @author Zakeria
 *
 */
public class TileConfig {
	/**
	 * The tile set id that this tile belongs to.
	 */
	private final int tilesetId;
	/**
	 * The texture id within a {@link TileSet} that this tile belongs to.
	 */
	private int textureId;
	/**
	 * The tile walkable flag.
	 */
	private final boolean walkable;

	public TileConfig(int tilesetId, int textureId, boolean walkable) {
		this.tilesetId = tilesetId;
		this.textureId = textureId;
		this.walkable = walkable;
	}

	/**
	 * Gets the tile set id this tile belongs to.
	 * 
	 * @return
	 */
	public int getTilesetId() {
		return tilesetId;
	}

	/**
	 * Gets the texture id this tile belongs to.
	 * 
	 * @return the texture id
	 */
	public int getTextureId() {
		return textureId;
	}

	/**
	 * Gets the walkable flag.
	 * 
	 * @return walkable - the flag
	 */
	public boolean isWalkable() {
		return walkable;
	}
	
	
	public void setTextureId(int textureId) {
		this.textureId = textureId;
	}
}
