package org.mobmmo.world.map;

/**
 * @author Zakeria
 *
 */
public final class MapConstants {
		
	private MapConstants(){}
	
	public static final int TILE_WIDTH = 64;
	
	public static final int TILE_HEIGHT = 32;
	
	public static final String MAP_DIR = "maps/";
	
	public static final int TILESET_COUNT = 2;

	public static final int MAP_COUNT = 1;	
}