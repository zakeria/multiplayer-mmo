package org.mobmmo.net;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.security.Key;
import java.security.interfaces.RSAPublicKey;
import java.util.logging.Logger;

import org.mobmmo.GameClient;
import org.mobmmo.net.packet.Packet;
import org.mobmmo.net.security.AESEncryption;
import org.mobmmo.net.security.RSAEncryption;

/**
 * Class responsible for handling the connection.
 * 
 * @author Zakeria
 *
 */
public final class Connection implements Runnable {
	/**
	 * A logger instance for this class.
	 */
	private final Logger logger = Logger.getLogger(Connection.class.getName());
	/**
	 * The {@link GameClient}.
	 */
	private final GameClient client;
	/**
	 * A {@link SocketChannel} used for creating a connection between the server
	 * and client.
	 */
	private SocketChannel socket;
	/**
	 * Indicates whether we are connected to the server.
	 */
	private boolean connected;
	/**
	 * Indicates whether the connection is ready to perform read operations.
	 */
	private boolean readReady;

	public Connection(GameClient client) {
		this.client = client;
	}

	/**
	 * Connects to the server.
	 * 
	 */
	public void connect() {
		try {
			logger.info("binding to port " + NetworkConstants.PORT);
			socket = SocketChannel.open();
			socket.configureBlocking(false);
			socket.connect(new InetSocketAddress(NetworkConstants.IP, NetworkConstants.PORT));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Writes outgoing data.
	 * 
	 * @param packet
	 *            the {@link Packet}.
	 */
	public void write(Packet packet) {
		Key key = client.getSettings().getAesKey();
		try {
			ByteBuffer buffer = ByteBuffer.allocate(1024);
			int size = packet.getBuffer().remaining();
			byte[] payload = new byte[size];
			packet.getBuffer().get(payload);
			byte[] encryptedPayload = AESEncryption.encrypt(payload, key);
			buffer.put((byte) packet.getId());
			buffer.putInt((byte) encryptedPayload.length);
			buffer.put(encryptedPayload);
			buffer.flip();
			socket.write(buffer);
		} catch (IOException e) {
			logger.info("an exception occured whilst writing");
		}
	}

	/**
	 * Exchanges the generated AES key with the server. This is done by
	 * encrypting the AES key using the {@link RSAPublicKey} generated for the
	 * server and client.
	 */
	public void exchangeKey() {
		try {
			System.out.println("called ex");
			Key aesKey = client.getSettings().getAesKey();
			RSAPublicKey publicKey = client.getSettings().getPublicKey();
			byte[] encryptedKey = RSAEncryption.encrypt(aesKey.getEncoded(), publicKey);
			ByteBuffer buffer = ByteBuffer.allocate(2048);
			buffer.put((byte) encryptedKey.length);
			buffer.put(encryptedKey);
			buffer.flip();
			socket.write(buffer);
		} catch (IOException e) {
			logger.info("an exception occured whilst writing");
		}
	}

	/**
	 * Read unsigned byte values using nio's {@link ByteBuffer}.
	 * 
	 * @return the unsigned byte as an integer 
	 */
	private int getUnsignedByte(ByteBuffer buffer) {
	   return (buffer.get()) & 0xff;
	}

	/**
	 * Reads incoming data.
	 */
	private void read() {
		try {
			// only read data when the connection is ready to perform read
			// operations.
			if (!readReady) {
				return;
			}
			// grab the AES key the client generated.
			Key key = client.getSettings().getAesKey();
			// allocate a new buffer with the potential size of
			// MAXIMUM_READ_LENGTH for this read operation, adjust accordingly.
			ByteBuffer buffer = ByteBuffer.allocate(NetworkConstants.MAXMUM_READ_LENGTH);

			// the length of the incoming message.
			int messageLength = socket.read(buffer);
			
			// an empty message, stop reading.
			// or a negative message, also stop reading.
			if (messageLength == 0 || messageLength < 0) {
				return;
			}

			System.out.println("size of message " + messageLength);
			// prepare the buffer for read/write operations.
			buffer.flip();
			// get all the data from the buffer.
			while (buffer.remaining() > 0) {
				// get the packet id.
				int packetId = getUnsignedByte(buffer);
				
				//login messages are dealth with separately.
				if(packetId == 1) {
					int response = buffer.get();
					int clientId = buffer.getInt();
					if(response == 1) {
						client.setId(clientId);
						client.setLoggedIn(true);
					}
					client.handleResponse(response);
					return;
				}
				
				// next we read the length of the packet.
				int length = buffer.getInt();

				byte[] encryptedPayload = new byte[length];
				buffer.get(encryptedPayload);
				
				// decrypt the data.
				byte[] decryptedPayload = AESEncryption.decrypt(encryptedPayload, key);
				// construct a packet with the information we obtained.
				Packet packet = new Packet(packetId, ByteBuffer.wrap(decryptedPayload));
				// queue the constructed packet, to be processed.
				client.getPacketManager().addIncoming(packet);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the socket channel.
	 * 
	 * @return the socket
	 */
	public SocketChannel getSocket() {
		return socket;
	}

	/**
	 * Gets the flag holding the connections status.
	 * 
	 * @return connected
	 */
	public boolean isConnected() {
		return connected;
	}

	public void setReadReady(boolean readReady) {
		this.readReady = readReady;
	}

	/**
	 * Runs this connection.
	 */
	@Override
	public void run() {
		try {
			// finish connecting.
			while (!socket.finishConnect()) {}
			connected = true;
			while (true) {
				try {
					if (connected) {
						read();
						client.getPacketManager().processIncomingPacket();
						client.getPacketManager().processOutgoingPackets();
					}
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
