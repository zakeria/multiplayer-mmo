package org.mobmmo.net;

import java.nio.ByteBuffer;

/**
 * @author Zakeria
 *
 */
public final class NetworkConstants {
	
	private NetworkConstants() {}

	public static final int PORT = 27015;

	public static final String IP = "10.209.246.229";
	/**
	 * The maximum amount of data in bytes that a {@link ByteBuffer} can read.
	 */
	public static final int MAXMUM_READ_LENGTH = 30000;
}
 