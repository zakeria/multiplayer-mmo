package org.mobmmo.net.packet.handlers;

import org.mobmmo.GameClient;
import org.mobmmo.net.packet.Packet;
import org.mobmmo.net.packet.PacketHandler;
/**
 * The player update packet, takes care of player updating, e.g. changes to appearance.
 * 
 * @author Zakeria
 *
 */
public final class PlayerUpdatePacket implements PacketHandler {

	@Override
	public void execute(Packet packet, GameClient client) {
		/**
		 * do player updating related actions here.
		 */
	}
}
