package org.mobmmo.net.packet.handlers;

import org.mobmmo.GameClient;
import org.mobmmo.entity.Entity;
import org.mobmmo.net.packet.Packet;
import org.mobmmo.net.packet.PacketHandler;
/**
 * Handles player movement.
 * 
 * @author Zakeria
 *
 */
public final class PlayerMovementPacket implements PacketHandler {

	@Override
	public void execute(Packet packet, GameClient client) {
		while (packet.getPayload().remaining() > 0) {
			int id = packet.readInt();
			int clear = packet.readByte();
			int x = packet.readByte();
			int y = packet.readByte();
			int dir = packet.readByte();
			int speed = packet.readByte();

			client.setTestX(x);
			client.setTestY(y);
			
			Entity p = client.getGameScreen().getWorld().getEntity(id);

			// player is not visible.
			if (p == null) {
				return;
			}
		
			if (client.getGameScreen().getWorld().getScreen().isLoadingRegion()) {
				client.getGameScreen().getWorld().getScreen().setLoadingRegion(false);
			}
			//clear the step manager.
			if (clear == 1) {
				p.getStepManager().clearSteps();
			}
			p.getStepManager().addStep(x, y, speed);
		}
	}
}
