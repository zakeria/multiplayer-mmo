package org.mobmmo.net.packet.handlers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.mobmmo.GameClient;
import org.mobmmo.entity.Entity;
import org.mobmmo.entity.LocalPlayer;
import org.mobmmo.entity.Player;
import org.mobmmo.net.packet.Packet;
import org.mobmmo.net.packet.PacketHandler;
import org.mobmmo.util.Misc;
import org.mobmmo.world.World;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

/**
 * A {@link PacketHandler} that deals with map region information being sent by
 * the server.
 * 
 * @author Zakeria
 *
 */
public final class RegionUpdatePacket implements PacketHandler {

	@Override
	public void execute(Packet packet, GameClient client) {
		World world = client.getGameScreen().getWorld();
		while (packet.getPayload().remaining() > 0) {
			int message = packet.readByte();
			if (message == 0) {
				addPlayer(packet, world, client);
			} else if (message == 1) {
				removePlayer(packet, world, client);
			} else if (message == 2) {
				world.getPlayer().getStepManager().clearSteps();
				world.getScreen().setLoadingRegion(true);
			} else {
				world.clearLocalPlayers();

			}
		}
	}

	/**
	 * Adds a new {@link Player} to the {@link World}.
	 * 
	 * @param packet
	 * @param world
	 * @param client
	 */
	private void addPlayer(Packet packet, World world, GameClient client) {
		int id = packet.readInt();
		String name = Misc.readString(packet.getPayload());
		int posX = packet.readByte();
		int posY = packet.readByte();

		List<Entity> entities = new ArrayList<>(world.getEntities());
		for (Entity p : entities) {
			if (id == world.getPlayer().getId()) {
				return;
			}
			if (p.getId() == id) {
				return;
			}
		}

		Vector2 worldPos = Misc.tileToWorld(posX, posY);
		Sprite sprite = new Sprite(world.getScreen().getAssetManager().get("characters/player.png", Texture.class));
		sprite.setPosition(worldPos.x, worldPos.y);
		LocalPlayer local = new LocalPlayer(id, sprite, world);
		local.setName(name);
		world.addEntity(local);
	}

	/**
	 * Removes a {@link Player} from the {@link World}.
	 * 
	 * @param packet
	 * @param world
	 * @param client
	 */
	private void removePlayer(Packet packet, World world, GameClient client) {
		int id = packet.readInt();
		Entity entity = world.getEntity(id);
		if (entity != null)
			world.removeEntity(entity);
	}
}
