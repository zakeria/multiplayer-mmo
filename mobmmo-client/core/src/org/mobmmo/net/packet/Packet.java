package org.mobmmo.net.packet;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import javax.sound.midi.Sequence;

/**
 * Represents a packet.
 * 
 * @author Zakeria
 *
 */
public final class Packet {
	/**
	 * The id of this packet.
	 */
	private final int id;

	/**
	 * The outgoing {@link ByteBuffer}.
	 */
	private ByteBuffer buffer;
	/**
	 * The payload {@link ByteBuffer}, containing an incoming message.
	 */
	private ByteBuffer payload;

	/**
	 * The UTF-8 character set {@link Charset}.
	 */
	private final Charset stringEncoder = Charset.forName("UTF-8");

	/**
	 * Creates an outgoing Packet.
	 * 
	 * @param id - the id of the Packet
	 */
	public Packet(int id) {
		this.id = id;
	}
	/**
	 * Creates an incoming Packet.
	 * @param id - the id of the packet.
	 * @param payload - the incoming payload.
	 */
	public Packet(int id, ByteBuffer payload) {
		this.id = id;
		this.payload = payload;
	}

	/**
	 * Allocates a new {@link ByteBuffer}.
	 * 
	 * @param size - the size of the buffer.
	 * @return this Packet.
	 */
	public Packet allocate(int size) {
		buffer = ByteBuffer.allocate(size);
		return this;
	}

	/**
	 * Put a {@link Sequence} of bytes in the outgoing buffer.
	 * 
	 * @param string
	 */
	public void putString(String string) {
		//the hash value is used as a terminator, which
		//denotes when the bytes of a String have been received. 
		char terminator = '#';
		string += terminator;
		buffer.put(string.getBytes(stringEncoder));
	}

	/**
	 * Put an integer value in the outgoing buffer.
	 * 
	 * @param i - int value
	 */
	public void putInt(int i) {
		buffer.putInt(i);
	}

	/**
	 * Put a byte value in the outgoing buffer.
	 * 
	 * @param b - byte value
	 */
	public void putByte(int b) {
		buffer.put((byte) b);
	}

	/**
	 * Read the next four bytes at the buffer's current position,
	 *  compose them into an int value.
	 *  @return the int value.
	 */
	public int readInt() {
		return payload.getInt();
	}
	/**
	 * Reads the byte at this buffer's current position.
	 * @return the byte value.
	 */
	public int readByte() {
		return payload.get();
	}
	/**
	 * Get the id of this Packet.
	 * @return id - the id.
	 */
	public int getId() {
		return id;
	}
	/**
	 * Get the outgoing {@link ByteBuffer}.
	 * @return buffer.
	 */
	public ByteBuffer getBuffer() {
		return buffer;
	}
	/**
	 * Get the payload {@link ByteBuffer}.
	 * @return payload.
	 */
	public ByteBuffer getPayload() {
		return payload;
	}
}
