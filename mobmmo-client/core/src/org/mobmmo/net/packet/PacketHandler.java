package org.mobmmo.net.packet;

import org.mobmmo.GameClient;

/**
 * @author Zakeria
 *
 */
public interface PacketHandler {

	public void execute(Packet packet, GameClient client);
}
