package org.mobmmo.net.packet;

import org.mobmmo.net.packet.handlers.PlayerMovementPacket;
import org.mobmmo.net.packet.handlers.PlayerUpdatePacket;
import org.mobmmo.net.packet.handlers.RegionUpdatePacket;

/**
 * @author Zakeria
 *
 */
public final class PacketConfiguration {
	/**
	 * A total of 255 packets can be used.
	 */
	private final PacketHandler[] configuration = new PacketHandler[256];

	/**
	 * Initialise packet handlers.
	 */
	public void loadHandlers() {
		configuration[2] = new PlayerMovementPacket();
		configuration[4] = new PlayerUpdatePacket();
		configuration[7] = new RegionUpdatePacket();
	}

	/**
	 * Get the packet handlers.
	 * @return configuration
	 */
	public PacketHandler[] getHandlers() {
		return configuration;
	}
}