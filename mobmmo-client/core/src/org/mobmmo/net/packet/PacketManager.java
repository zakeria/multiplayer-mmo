package org.mobmmo.net.packet;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.mobmmo.GameClient;

/**
 * @author Zakeria
 *
 */
public final class PacketManager {

	/**
	 * A {@link ConcurrentLinkedQueue} containing incoming packets.
	 */
	private final Queue<Packet> incomingPackets = new ConcurrentLinkedQueue<Packet>();
	
	/**
	 * A {@link ConcurrentLinkedQueue} containing outgoing packets.
	 */
	private final Queue<Packet> outgoingPackets = new ConcurrentLinkedQueue<Packet>();
	
	/**
	 * The packet configuration.
	 */
	private final PacketConfiguration configuration;
	/**
	 * An instance of {@link GameClient}.
	 */
	private final GameClient client;
	
	public PacketManager(GameClient client) {
		this.client = client;
		this.configuration = new PacketConfiguration();
	}

	public void loadConfiguration() {
		configuration.loadHandlers();
	}
	
	/**
	 * Adds a packet that is to be processed.
	 * 
	 * @param p
	 */
	public void addIncoming(Packet p) {
		incomingPackets.add(p);
	}
	/**
	 * Adds a packet that is to be processed.
	 * 
	 * @param p
	 */
	public void addOutGoing(Packet p) {
		outgoingPackets.add(p);
	}

	/**
	 * Processes a queued incoming {@link Packet}.
	 */
	public synchronized void processIncomingPacket() {
		while (incomingPackets.peek() != null) {
			Packet p = incomingPackets.poll();
			handle(p);
		}
	}
	
	/**
	 * Processes a queued outgoing {@link Packet}.
	 */
	public synchronized void processOutgoingPackets() {
		while (outgoingPackets.peek() != null) {
			Packet p = outgoingPackets.poll();
			client.getConnection().write(p);
		}
	}
	
	/**
	 * Executes a {@link PacketHandler}.
	 * @param p
	 */
	public void handle(Packet p) {
		PacketHandler handler = configuration.getHandlers()[p.getId()];
		handler.execute(p, client);
	} 
}
