/**
 * Contains network security related classes for the game client.
 */
/**
 * @author Zakeria
 *
 */
package org.mobmmo.net.security;