package org.mobmmo.net.security;

import java.security.interfaces.RSAPublicKey;

/**
 * A class for storing security related constants.
 * 
 * @author Zakeria
 *
 */
public final class SecurityConstants {

	/**
	 * The latest generated {@link RSAPublicKey} exponent.
	 */
	public static final String RSA_PUBLIC_EXPONENT = "65537";

	/**
	 * The latest generated {@link RSAPublicKey} modulus.
	 */
	public static final String RSA_PUBLIC_MODULES = "92252287725858534705620972795849771399867275380449270371878235763807583670631757575862770673482161645891383895401421726179120259935769188763844092950868386367870550928269218785959132725636400433110237476998373828827381517726003147961846406326431833592471970625351235496738937114013683462512863738680643924967";
}
