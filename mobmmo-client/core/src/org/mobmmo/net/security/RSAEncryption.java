package org.mobmmo.net.security;

import java.security.interfaces.RSAPublicKey;

import javax.crypto.Cipher;

/**
 * A class that handles RSA encryption of byte arrays.
 * 
 * @author Zakeria
 *
 */
public final class RSAEncryption {
	/**
	 * Encrypts a byte array using the RSA public key. This method is used to
	 * encrypt the AES key such that it can be transported safely from the
	 * client to the server. Once this is done, we will use a symmetric with AES
	 * encryption to encrypt all game packets.
	 * 
	 * @param data
	 *            the data being encrypted.
	 * @param key
	 *            the {@link RSAPublicKey}.
	 * @return encryptedData - the encrypted data.
	 */
	public static byte[] encrypt(byte[] data, RSAPublicKey key) {
		byte[] encryptedData = null;
		try {
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			encryptedData = cipher.doFinal(data);
		} catch (Exception e) {
		}
		return encryptedData;
	}
}
