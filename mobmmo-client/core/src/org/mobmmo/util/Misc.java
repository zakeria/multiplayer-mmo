package org.mobmmo.util;

import java.nio.ByteBuffer;

import org.mobmmo.screen.GameScreen;
import org.mobmmo.world.map.Tile;

import com.badlogic.gdx.math.Vector2;

/**
 * A class for miscellaneous functions.
 * 
 * @author Zakeria
 */
public final class Misc {

	/**
	 * The string terminator used by the client-server.
	 */
	private static final char STRING_TERMINATOR = '#';

	/**
	 * Converts a {@link Tile} position to a world position. Same process as the
	 * rendering in {@link GameScreen}. 
	 * 64 and 32 are the tile height/widths.
	 * 
	 * @param x
	 *            the tile x
	 * @param y
	 *            the tile y
	 */
	public static Vector2 tileToWorld(int x, int y) {
		int x_pos = (int) ((x * 64 / 2.0f) + (y * 64 / 2.0f));
		int y_pos = (int) (-(x * 32 / 2.0f) + (y * 32 / 2.0f));
		Vector2 position = new Vector2(x_pos, y_pos);
		return position;
	}

	/**
	 * Reads a string from the specified {@link ByteBuf}.
	 *
	 * @param buffer
	 *            A {@link ByteBuffer}.
	 * @return string the String
	 */
	public static String readString(ByteBuffer buffer) {
		StringBuilder builder = new StringBuilder();
		int character;
		while (buffer.hasRemaining() && (character = buffer.get()) != STRING_TERMINATOR) {
			builder.append((char) character);
		}
		return builder.toString();
	}
}
