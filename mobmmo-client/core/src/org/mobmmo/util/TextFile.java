package org.mobmmo.util;

import com.badlogic.gdx.files.FileHandle;

/**
 * @brief Represents a text file used for loading map files.
 * 
 * @author Zakeria
 *
 */
public final class TextFile {

	/**
	 * The text data in bytes.
	 */
	private final byte[] data;

	public TextFile(byte[] data) {
		this.data = data;
	}

	public TextFile(FileHandle file) {
		this.data = file.readBytes();
	}

	/**
	 * Gets the byte array.
	 * @return the data
	 */
	public byte[] getByteArray() {
		return data;
	}
}