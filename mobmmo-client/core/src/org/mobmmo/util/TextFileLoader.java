package org.mobmmo.util;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

/**
 * A loader for text files.
 * 
 * @author Zakeria
 *
 */
public final class TextFileLoader extends AsynchronousAssetLoader<TextFile, TextFileLoader.TextParameter> {

	private TextFile textFile;

	public TextFileLoader(FileHandleResolver resolver) {
		super(resolver);
	}

	@Override
	public void loadAsync(AssetManager manager, String fileName, FileHandle file, TextParameter parameter) {
		this.textFile = new TextFile(file);
	}

	@Override
	public TextFile loadSync(AssetManager manager, String fileName, FileHandle file, TextParameter parameter) {
		TextFile text = this.textFile;
		return text;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, TextParameter parameter) {
		return null;
	}

	public static class TextParameter extends AssetLoaderParameters<TextFile> {
	}
}