package org.mobmmo.test;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mobmmo.GameClient;
import org.mobmmo.net.Connection;

/**
 * @author Zakeria
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public final class TestFramework {

	/**
	 * Client instance.
	 */
	private static GameClient client;

	/**
	 * Set up the test.
	 */
	@BeforeClass
	public static void beforeClass1() {
		client = new GameClient();
		new Thread(client).start();
		while (!client.getConnection().isConnected()) {
			System.out.println("connecting...");
		} System.out.println("connection established");

		Scanner scanner = new Scanner(System.in);
		System.out.println("press any key to exchange keys");
		String response = scanner.next();
		client.getConnection().exchangeKey();

		System.out.println("press any key to login");
		String response2 = scanner.next();

		client.login();
		while (!client.isLoggedIn()) {
			System.out.println("not logged in");
		}
	}

	@Test
	public void test1() {// test connected.
		Connection connection = client.getConnection();
		assertTrue(connection.isConnected());
	}

	/**
	 * Tests if the client is logged in.
	 */
	@Test
	public void test2() {
		assertTrue(client.isLoggedIn());
	}

	// class used to compare positions.
	private static final class Position {
		final int x;
		final int y;

		public Position(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Position) {
				Position other = (Position) obj;
				if (other.getX() == x && (other.y == y)) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * Test if a player has moved.
	 */
	@Test
	public void test3() {
		client.requestWalk(5, 5);
		Position oldPosition = new Position(client.getTestX(), client.getTestY());
		
		// allow time for results
		while (true) {
			try {
				Thread.sleep(4000);
				break;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Position newPosition = new Position(client.getTestX(), client.getTestY());
		
		assertFalse(oldPosition.equals(newPosition));
	}
}
