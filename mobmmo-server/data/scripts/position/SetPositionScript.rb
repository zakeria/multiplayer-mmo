# A script that sets the client position of a player.
require 'java'

java_import 'org.mobmmo.script.Script'
java_import 'org.mobmmo.entity.Player'
java_import 'org.mobmmo.entity.EntityPosition'

class SetPositionScript < Script
   
  def id
  	0
  end

  def name
  	"Set_Position"
  end

  def author
  	"Zakeria"
  end

  def execute(args)
  	player = args[0]
  	x = args[1]
  	y = args[2]
  	player.setClientPosition(EntityPosition.new(x,y))
  end

end  