package org.mobmmo;

import java.util.logging.Logger;

import org.mobmmo.net.packet.PacketManager;
import org.mobmmo.script.ScriptManager;
import org.mobmmo.script.ScriptingEngine;
import org.mobmmo.world.World;

/**
 * @author Zakeria 
 * 
 */
public final class ServerManager implements Runnable {

	/**
	 * An instance of {@link Server}.
	 */
	private final Server server;
	/**
	 * The {@link ScriptingEngine}.
	 */
	private final ScriptingEngine scripEngine;
	/**
	 * The {@link ScriptManager}.
	 */
	private final ScriptManager scriptManager;
	/**
	 * The {@link PacketManager}.
	 */
	private final PacketManager packetManager;
	/**
	 * The {@link World}.
	 */
	private World world;
	/**
	 * The logger instance for this class - use log4j already.
	 */
	private final Logger logger = Logger.getLogger(ServerManager.class.getName());

	public ServerManager(Server server) {
		this.server = server;
		this.packetManager = new PacketManager(this);
		this.scripEngine = new ScriptingEngine();
		this.scriptManager = new ScriptManager();
	}

	/**
	 * Loads server configuration and starts the server game thread.
	 */
	public void startGameServer() {
		logger.info("Starting server manager...");

		world = new World(this);
		world.loadWorld();
		scripEngine.initialise();
		packetManager.loadConfiguration();

		new Thread(this).start();
	}

	/**
	 * Get the {@link PacketManager}.
	 * 
	 * @return the packet manager
	 */
	public PacketManager getPacketManager() {
		return packetManager;
	}

	/**
	 * Get the {@link World}.
	 * 
	 * @return
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * Get the {@link ScriptingEngine}.
	 * 
	 * @return script engine
	 */
	public ScriptingEngine getScriptEngine() {
		return scripEngine;
	}

	/**
	 * Get the {@link ScriptManager}.
	 * 
	 * @return scriptManager
	 */
	public ScriptManager getScriptManager() {
		return scriptManager;
	}

	/**
	 * Get {@link Server).
	 * 
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	@Override
	public void run() {
		while (true) {
			try {
				packetManager.processIncomingPackets();
				packetManager.processOutgoingPackets();
				world.updateGameWorld();
				Thread.sleep(Settings.SERVER_TICK);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}