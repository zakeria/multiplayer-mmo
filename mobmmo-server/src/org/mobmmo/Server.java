package org.mobmmo;

import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.logging.Logger;

import org.mobmmo.net.NetworkConstants;
import org.mobmmo.net.PipelineFactory;
import org.mobmmo.net.security.SecurityConstants;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * @author Zakeria
 * 
 */ 
public final class Server {

	/**
	 * The {@link Logger} instance for this class.
	 */
	private final Logger logger = Logger.getLogger(Server.class.getName());

	private final EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
	private final ServerManager serverManager;
	private ServerBootstrap serverBootstrap;

	private Settings settings;

	public Server() {
		this.serverManager = new ServerManager(this);
	}

	public static void main(String[] args) {
		Server server = new Server();
		server.initialise();
		server.start();
		server.bind(new InetSocketAddress(NetworkConstants.PORT));
		server.serverManager.startGameServer();
	}

	private void initialise() {
		try {
			KeyFactory factory = KeyFactory.getInstance("RSA");
			BigInteger privateModulus = new BigInteger(SecurityConstants.RSA_PRIVATE_MODULES);
			BigInteger privateExponent = new BigInteger(SecurityConstants.RSA_PRIVATE_EXPONENT);
			RSAPrivateKeySpec privateSpec = new RSAPrivateKeySpec(privateModulus, privateExponent);

			BigInteger publicModulus = new BigInteger(SecurityConstants.RSA_PUBLIC_MODULES);
			BigInteger publicExponent = new BigInteger(SecurityConstants.RSA_PUBLIC_EXPONENT);
			RSAPublicKeySpec publicSpec = new RSAPublicKeySpec(publicModulus, publicExponent);

			RSAPrivateKey privKey = (RSAPrivateKey) factory.generatePrivate(privateSpec);
			RSAPublicKey pubKey = (RSAPublicKey) factory.generatePublic(publicSpec);
			settings = new Settings(factory, privKey, pubKey);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Stars the game server. This method initialises the channel handlers and
	 * prepares the server for incoming messages.
	 */
	public void start() {
		logger.info("starting server...");
		ChannelInitializer<SocketChannel> channelInitializer = new PipelineFactory(this);
		serverBootstrap = new ServerBootstrap();
		serverBootstrap.group(eventLoopGroup);
		serverBootstrap.channel(NioServerSocketChannel.class);
		serverBootstrap.childHandler(channelInitializer);
	}

	/**
	 * Binds the server channel using the {@link ServerBootstrap}.
	 * 
	 * @param address
	 *            - the port
	 */
	public void bind(SocketAddress address) {
		try {
			logger.info("binding on " + address);
			serverBootstrap.bind(address).sync();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the {@link ServerManager}
	 * 
	 * @return serverManager
	 */
	public ServerManager getServerManager() {
		return serverManager;
	}

	public Settings getSettings() {
		return settings;
	}
}
