package org.mobmmo;

import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

/**
 * @author Zakeria
 *
 */
public final class Settings {
	/**
	 * The game title.
	 */
	public static final String TITLE = "MobMMO";

	/**
	 * The server tick rate.
	 */
	public static final int SERVER_TICK = 500;

	/**
	 * An array of the packmet ids used by the server <-> client.
	 */
	public static final int[] PACKET_IDS = { 1, 2, 3, 4, 5, 6, 7, 8 };

	/**
	 * The maximum amount of players that can be online and any given time.
	 */
	public static final int MAX_PLAYERS = 1000;

	/**
	 * The script directory.
	 */
	public static final String SCRIPT_DIR = "data/scripts";

	/**
	 * The {@link KeyFactory}.
	 */
	private final KeyFactory factory;
	/**
	 * The private RSA key.
	 */
	private final RSAPrivateKey privateKey;
	/**
	 * The public RSA key.
	 */
	private final RSAPublicKey publicKey;

	public Settings(KeyFactory factory, RSAPrivateKey privateKey, RSAPublicKey publicKey) {
		this.factory = factory;
		this.privateKey = privateKey;
		this.publicKey = publicKey;
	}

	/**
	 * Get the key factory.
	 * 
	 * @return factory
	 */
	public KeyFactory getKeyFactory() {
		return factory;
	}

	/**
	 * Get the private key.
	 * 
	 * @return privateKey
	 */
	public RSAPrivateKey getPrivateKey() {
		return privateKey;
	}

	/**
	 * Get the public key.
	 * 
	 * @return publicKey
	 */
	public RSAPublicKey getPublicKey() {
		return publicKey;
	}
}
