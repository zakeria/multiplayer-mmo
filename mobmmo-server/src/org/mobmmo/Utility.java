package org.mobmmo;

import org.mobmmo.entity.Entity;
import org.mobmmo.entity.EntityPosition;

import io.netty.buffer.ByteBuf;

/**
 * A class for utility functions.
 * 
 * @author Zakeria
 */
public final class Utility {
		
	/**
	 * Reads a string from the specified {@link ByteBuf}.
	 *
	 * @param buffer The buffer.
	 * @return The string.
	 */
	public static String readString(ByteBuf buffer) {
		StringBuilder builder = new StringBuilder();
		int character;
		char terminator = '#';
		while (buffer.isReadable() && (character = buffer.readUnsignedByte()) != terminator) {
			builder.append((char) character);
		}
		return builder.toString();
	}
	
	/**
	 * Converts a tile position to a world position.
	 * @param x
	 * @param y
	 * @return
	 */
	public static EntityPosition tileToWorld(int x, int y) {
		int x_pos = (int) ((x * 64 / 2.0f) + (y * 64 / 2.0f));
		int y_pos = (int) (-(x * 32 / 2.0f) + (y * 32 / 2.0f));
		EntityPosition position = new EntityPosition(x_pos, y_pos);
		return position;
	}
	
	/**
	 * Gets the distance between two entities.
	 * @param e1
	 * @param e2
	 * @return
	 */
	public static int distanceBetween(Entity e1, Entity e2) {
		EntityPosition e1Pos = tileToWorld(e1.getPosition().getX(), e1.getPosition().getY());
		EntityPosition e2Pos = tileToWorld(e2.getPosition().getX(), e2.getPosition().getY());

		int x = (int) Math.pow((int)e1Pos.getX() - (int)e2Pos.getX() , 2D);
		int y = (int) Math.pow((int)e1Pos.getY()  -(int)e2Pos.getY(), 2D);
		return (int) Math.floor(Math.sqrt(x + y));
	}
}
