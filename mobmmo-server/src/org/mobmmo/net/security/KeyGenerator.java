package org.mobmmo.net.security;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

/**
 * A class that handles the generation of keys used for various encryption
 * algorithms.
 * 
 * @author Zakeria
 *
 */
public final class KeyGenerator {
	/**
	 * Generates a public and private key pair for RSA encryption.
	 * 
	 * @return pair - the generated {@link KeyPair}.
	 */
	public static KeyPair generateRSAKeys() {
		KeyPair pair = null;
		try {
			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
			generator.initialize(1024);
			pair = generator.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return pair;
	}

	/**
	 * Generates a 16 byte AES key to be used for encryption.
	 * 
	 * @return key - the generated {@link Key}.
	 */
	public static Key generateAESKey() {
		Key key = null;
		javax.crypto.KeyGenerator generator;
		try {
			generator = javax.crypto.KeyGenerator.getInstance("AES");
			generator.init(128);
			key = generator.generateKey();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return key;
	}
}
