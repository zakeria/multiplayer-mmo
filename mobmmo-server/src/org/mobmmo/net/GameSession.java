package org.mobmmo.net;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;

import java.util.logging.Logger;
import org.mobmmo.ServerManager;
import org.mobmmo.entity.Player;

/**
 * @author Zakeria
 *
 */
@Sharable
public final class GameSession extends ChannelHandlerAdapter {

	/**
	 * Logger instance for this class.
	 */
	private final Logger logger = Logger.getLogger(GameSession.class.getName());

	/**
	 * The channel belonging to this session.
	 */
	private final Channel channel;
	/**
	 * The {@link Player} this session is for.
	 */
	private Player player;

	/**
	 * The {@link ServerManager}.
	 */
	private final ServerManager serverManager;

	public GameSession(Channel channel, ServerManager serverManager) {
		this.channel = channel;
		this.serverManager = serverManager;
	}

	@Override
	public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
		logger.info("New Session from " + ctx.channel().remoteAddress());
	}

	@Override
	public void channelInactive(ChannelHandlerContext context) {
		Channel channel = context.channel();
		channel.close();
		player.setSessionTerminated(true);
	}

	@Override
	public void channelRead(ChannelHandlerContext context, Object message) {
	}

	/**
	 * Get the {@link Channel}.
	 * 
	 * @return the channel
	 */
	public Channel getChannel() {
		return channel;
	}

	public ServerManager getServerManager() {
		return serverManager;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
}
