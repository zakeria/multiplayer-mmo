package org.mobmmo.net;

import org.mobmmo.Server;
import org.mobmmo.ServerManager;
import org.mobmmo.net.codec.KeyMessageDecoder;
import org.mobmmo.net.codec.LoginMessageDecoder;
import org.mobmmo.net.codec.LoginMessageEncoder;
import org.mobmmo.net.codec.PacketMessageDecoder;
import org.mobmmo.net.codec.PacketMessageEncoder;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

/**
 * @author Zakeria
 *
 */
public final class PipelineFactory extends ChannelInitializer<SocketChannel> {
	/**
	 * An instance of the {@link Server}.
	 */
	private final Server server;

	/**
	 * Create the pipleline factor.
	 * 
	 * @param server
	 */
	public PipelineFactory(Server server) {
		this.server = server;
	}

	/**
	 * Initialise each of the stages within this pipeline factory.
	 */
	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ServerManager serverManager = server.getServerManager();
		ChannelPipeline channelPipeline = ch.pipeline();
		channelPipeline.addLast("keyMessage", new KeyMessageDecoder(serverManager));
		channelPipeline.addLast("encoder", new LoginMessageEncoder(serverManager));
		channelPipeline.addLast("decoder", new LoginMessageDecoder(serverManager));
		channelPipeline.addLast("packetMessageEncoder", new PacketMessageEncoder(serverManager));
		channelPipeline.addLast("packetMessageDecoder", new PacketMessageDecoder(serverManager));
	}
}
