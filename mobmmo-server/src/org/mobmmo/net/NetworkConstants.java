package org.mobmmo.net;

import java.security.Key;

import io.netty.util.AttributeKey;

/**
 * @author Zakeria
 *
 */
public final class NetworkConstants {

	/**
	 * The server-client port.
	 */
	public static final int PORT = 27015;
	
	
	public static final AttributeKey<Key> AES_KEY = AttributeKey.valueOf("AESKey");

}
