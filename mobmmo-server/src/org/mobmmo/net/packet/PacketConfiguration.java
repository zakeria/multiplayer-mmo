package org.mobmmo.net.packet;

import org.mobmmo.net.packet.handlers.LocationHandler;
import org.mobmmo.net.packet.handlers.WalkingHandler;

/**
 * @author Zakeria
 *
 */
public final class PacketConfiguration {
	/**
	 * The packet handlers.
	 */
	private final PacketHandler[] handlers = new PacketHandler[256];

	/**
	 * Loads the packet handlers.
	 */
	public void loadHandlers() {
		handlers[2] = new WalkingHandler();
		handlers[3] = new LocationHandler();
	}

	public PacketHandler[] getHandlers() {
		return handlers;
	}
}
