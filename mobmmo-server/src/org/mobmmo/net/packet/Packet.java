package org.mobmmo.net.packet;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.charset.Charset;
import java.util.Arrays;

/**
 * Represents a packet.
 * 
 * @author Zakeria
 *
 */
public final class Packet {
	/**
	 * The id of this packet.
	 */
	private final int id;
	private final Charset stringEncoder = Charset.forName("UTF-8");
	/**
	 * The outgoing {@link ByteBuf}.
	 */
	private ByteBuf buffer;
	/**
	 * The payload of this packet(incoming).
	 */
	private ByteBuf payload;

	public Packet(int id) {
		this.id = id;
		this.buffer = Unpooled.buffer();
	}

	public Packet(int id, ByteBuf payload) {
		this.id = id;
		this.payload = payload;
	}

	/**
	 * Puts a string into the buffer, I am using the hash value to denote a string
	 * terminator.
	 * 
	 * @param string
	 */
	public void putString(String string) {
		string = string + '#';
		buffer.writeBytes(string.getBytes(stringEncoder));
	}
	/**
	 * Puts an integer into the buffer.
	 * @param i
	 */
	public void putInt(int i) {
		buffer.writeInt(i);
	}
	/**
	 * Puts a float into the buffer
	 * @param f
	 */
	public void putFloat(float f) {
		buffer.writeFloat(f);
	}
	/**
	 * Puts a long into the buffer
	 * @param f
	 */
	public void putLong(long l) {
		buffer.writeLong(l);
	}
	/**
	 * Puts a byte into the buffer
	 * @param f
	 */
	public void putByte(int b) {
		buffer.writeByte((byte) b);
	}
	/**
	 * Puts a short into the buffer
	 * @param f
	 */
	public void putShort(int s) {
		buffer.writeShort(s);
	}

	public int readInt() {
		return payload.readInt();
	}

	public long readLong() {
		return payload.readLong();
	}

	public int readByte() {
		return payload.readByte();
	}

	public short readShort() {
		return payload.readShort();
	}
	/**
	 * Gets the buffer.
	 * @return buffer
	 */
	public ByteBuf getBuffer() {
		return buffer;
	}

	/**
	 * Puts the payload into the buffer.
	 * 
	 * @param b
	 */
	public void putPayload(byte[] b) {
		buffer.writeBytes(b);
	}

	/**
	 * {@link ByteBuf} to byte array.
	 * 
	 * @return the byte array
	 */
	public byte[] toByteArray() {
		if (buffer.hasArray()) {
			return Arrays.copyOfRange(buffer.array(), 0, buffer.writerIndex());
		}
		throw new IllegalStateException("error copying bytes");
	}
	/**
	 * Gets the packet id.
	 * @return id
	 */
	public int getId() {
		return id;
	}
}
