package org.mobmmo.net.packet;

import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.mobmmo.ServerManager;

import io.netty.channel.Channel;

/**
 * @author Zakeria
 *
 */
public final class PacketManager {

	/**
	 * A {@link ConcurrentLinkedQueue} containing incoming packets.
	 */
	private final Queue<PacketData> incomingPackets = new ConcurrentLinkedQueue<>();
	/**
	 * A {@link ConcurrentLinkedQueue} containing outgoing packets.
	 */
	private final Queue<PacketData> outgoingPackets = new ConcurrentLinkedQueue<>();

	/**
	 * The packet configuration.
	 */
	private final PacketConfiguration configuration;

	/**
	 * The {@link ServerManager}.
	 */
	private final ServerManager serverManager;
	
	public PacketManager(ServerManager serverManager) {
		this.serverManager = serverManager;
		this.configuration = new PacketConfiguration();
	}

	/**
	 * Load packet configurations.
	 */
	public void loadConfiguration() {
		configuration.loadHandlers();
	}

	/**
	 * Adds a {@link Packet} to the incoming packets queue.
	 * 
	 * @param p
	 *            the packet
	 */
	public synchronized void addIncomingPacket(PacketData data) {
		incomingPackets.add(data);
	}

	/**
	 * Adds a {@link Packet} to the outgoing packets queue.
	 * 
	 * @param p
	 *            the packet
	 */
	public void addOutgoingPacket(PacketData data) {
		outgoingPackets.add(data);
	}

	/**
	 * Pekks an incoming packet if present.
	 * 
	 * @return the incoming packet
	 */
	private Optional<PacketData> peekIncomingPacket() {
		return Optional.ofNullable(incomingPackets.peek());
	}

	/**
	 * Peeks an outgoing packet if present.
	 * 
	 * @return the outgoing packet
	 */
	private Optional<PacketData> peekOutgoingPacket() {
		return Optional.ofNullable(outgoingPackets.peek());
	}

	/**
	 * Processes a queued incoming Packet.
	 */
	public synchronized void processIncomingPackets() {
		while (peekIncomingPacket().isPresent()) {
			PacketData data = peekIncomingPacket().get();
			data = incomingPackets.poll();
			handle(data);
		}
	}

	/**
	 * Processes a queued outgoing Packet.
	 */
	public synchronized void processOutgoingPackets() {
		while (peekOutgoingPacket().isPresent()) {
			PacketData data = peekOutgoingPacket().get();
			Channel channel = data.getSession().getChannel();
			data = outgoingPackets.poll();
			channel.write(data.getPacket());
		}
	}

	/**
	 * Executes a {@link PacketHandler}.
	 * 
	 * @param ata the packet data
	 * @param channel the channel
	 */
	public void handle(PacketData data) {
		Packet p = data.getPacket();
		PacketHandler handler = configuration.getHandlers()[p.getId()];
		handler.execute(serverManager, data);
	}
}
