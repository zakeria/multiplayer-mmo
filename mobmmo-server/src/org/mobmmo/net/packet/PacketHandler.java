package org.mobmmo.net.packet;

import org.mobmmo.ServerManager;

/**
 * @author Zakeria
 *
 */
public interface PacketHandler {

	/**
	 * Executes a {@link Packet}.
	 * 
	 * @param packet
	 *            the packet
	 */
	public abstract void execute(ServerManager serverManager, PacketData data);
}
