package org.mobmmo.net.packet;

import org.mobmmo.net.GameSession;

/**
 * 
 * @author Zakeria
 *
 */
public final class PacketData {

	/**
	 * The {@link GameSession}.
	 */
	private final GameSession session;

	/**
	 * The {@link Packet}.
	 */
	private final Packet packet;

	public PacketData(GameSession session, Packet packet) {
		this.session = session;
		this.packet = packet;
	}

	/**
	 * Gets the packet
	 * 
	 * @return packet
	 */
	public Packet getPacket() {
		return packet;
	}

	/**
	 * Gets the game session.
	 * 
	 * @return session
	 */
	public GameSession getSession() {
		return session;
	}

}
