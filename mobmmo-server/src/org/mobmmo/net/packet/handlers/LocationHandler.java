package org.mobmmo.net.packet.handlers;

import java.util.Optional;

import org.mobmmo.ServerManager;
import org.mobmmo.entity.Player;
import org.mobmmo.net.packet.Packet;
import org.mobmmo.net.packet.PacketData;
import org.mobmmo.net.packet.PacketHandler;
import org.mobmmo.script.Script;
import org.mobmmo.script.ScriptManager;
/**
 * @author Zakeria
 *
 */
public class LocationHandler implements PacketHandler {

	@Override
	public void execute(ServerManager serverManager, PacketData data) {
		Player player = data.getSession().getPlayer();
		Packet packet = data.getPacket();

		int x = packet.readByte();
		int y = packet.readByte();
		
		Optional<Script> script = Optional.ofNullable(ScriptManager.getScript(0));
		script.ifPresent(t -> t.execute(player, x, y));
	}
}
  