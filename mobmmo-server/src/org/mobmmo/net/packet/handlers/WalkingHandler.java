package org.mobmmo.net.packet.handlers;

import java.util.Optional;

import org.mobmmo.ServerManager;
import org.mobmmo.entity.EntityPosition;
import org.mobmmo.entity.Player;
import org.mobmmo.net.GameSession;
import org.mobmmo.net.packet.Packet;
import org.mobmmo.net.packet.PacketData;
import org.mobmmo.net.packet.PacketHandler;
import org.mobmmo.world.WorldMap;
import org.mobmmo.world.pf.AstarPathFinder;
import org.mobmmo.world.pf.Path;

/**
 * Handles the walking Packet.
 * 
 * @author Zakeria
 *
 */
public final class WalkingHandler implements PacketHandler {

	@Override
	public void execute(ServerManager serverManager, PacketData data) {
		//get the session that sent the message (if they are still present).
		Optional<GameSession> p = Optional.ofNullable(data.getSession());
		if(!p.isPresent())  {
			return;
		}
		Player player = p.get().getPlayer();
		Packet packet = data.getPacket();

		player.getStepManager().clear();
		player.getStepManager().setClearRequired(true);

		int x = packet.readByte();
		int y = packet.readByte();

		//calculate the path.
		WorldMap worldMap = serverManager.getWorld().getWorldMap();
		Path path = new AstarPathFinder().getPath(worldMap, player, new EntityPosition(x, y));

		//add the steps to the step manager.
		for (EntityPosition pos : path.getPath()) {
			player.getStepManager().addStep(pos);
		}
	}
}
