package org.mobmmo.net.codec;

import java.util.List;

import org.mobmmo.ServerManager;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

/**
 * @author Zakeria
 *
 */
public final class LoginMessageEncoder extends MessageToMessageEncoder<LoginMessageState> {

	private final ServerManager serverManager;

	public LoginMessageEncoder(ServerManager serverManager) {
		this.serverManager = serverManager;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, LoginMessageState msg, List<Object> out) throws Exception {
		ByteBuf buf = ctx.alloc().buffer(64);

		// packet id
		buf.writeByte(1);
		buf.writeByte(msg.getState());
		if (msg.getState() == 1)
		buf.writeInt(msg.getSessionId());
	
		ctx.channel().writeAndFlush(buf);
		//remove this class from the pipeline.
		ctx.pipeline().remove(this);
	}
}
