package org.mobmmo.net.codec;

import java.security.Key;
import java.util.List;

import org.mobmmo.ServerManager;
import org.mobmmo.net.GameSession;
import org.mobmmo.net.NetworkConstants;
import org.mobmmo.net.packet.Packet;
import org.mobmmo.net.packet.PacketData;
import org.mobmmo.net.security.AESEncryption;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

/**
 * @author Zakeria
 *
 */
public final class PacketMessageDecoder extends ByteToMessageDecoder {
	/**
	 * An instance of the {@link ServerManager}.
	 */
	private final ServerManager serverManager;

	public PacketMessageDecoder(ServerManager engine) {
		this.serverManager = engine;
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		if (in.readableBytes() >= 1) {
			// the id of the packet.
			int id = in.readByte();
			// if the id of the packet is not stored in the settings class, the
			// packet does not exist.
		
			// the length of the incoming message.
			int messageLength = in.readInt();

			if (in.readableBytes() >= messageLength) {
				ByteBuf encryptedPayload = in.readBytes(messageLength);
				// write the data onto this byte array.
				byte[] encrypted = new byte[messageLength];
				encryptedPayload.readBytes(encrypted);

				// grab this channels AES key and decrypt the payload.
				Key key = ctx.channel().attr(NetworkConstants.AES_KEY).get();
				byte[] decrypted = AESEncryption.decrypt(encrypted, key);
				// transfer the data onto a decrypted bytebuffer so the
				// remaining message can be read.
				ByteBuf decryptedPayload = Unpooled.wrappedBuffer(decrypted);
				GameSession session = (GameSession) ctx.pipeline().get("session");

				// construct the incoming packet using the decrypted data.
				Packet packet = new Packet(id, decryptedPayload);
				PacketData data = new PacketData(session, packet);

				// add the packet to the incoming packets queue, which will be
				// processed and sent every game tick (500 ms).
				serverManager.getPacketManager().addIncomingPacket(data);
			}
		}
	}
}
