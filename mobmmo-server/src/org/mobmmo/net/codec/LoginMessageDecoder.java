package org.mobmmo.net.codec;

import java.security.Key;
import java.util.List;

import org.mobmmo.ServerManager;
import org.mobmmo.Settings;
import org.mobmmo.Utility;
import org.mobmmo.entity.EntityDirection;
import org.mobmmo.entity.EntityPosition;
import org.mobmmo.entity.Player;
import org.mobmmo.entity.PlayerDetails;
import org.mobmmo.net.GameSession;
import org.mobmmo.net.NetworkConstants;
import org.mobmmo.net.security.AESEncryption;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

/**
 * Handles the login request.
 * 
 * @author Zakeria
 *
 */
public final class LoginMessageDecoder extends ByteToMessageDecoder {
	/**
	 * An instance of the {@link ServerManager}.
	 */
	private final ServerManager serverManager;
	/**
	 * Login was successful.
	 */
	private static final int SUCCESS = 1;
	/**
	 * The {@link Player} is already online.
	 */
	private static final int ALREADY_ONLINE = 2;
	/**
	 * The server has reached its player capacity.
	 */
	private static final int SERVER_FULL = 3;

	public LoginMessageDecoder(ServerManager serverManager) {
		this.serverManager = serverManager;
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		// the id of the packet.
		int id = in.readByte();
		// we only use the id value of 1 for login messages.
		if (id != 1) {
			in.clear();
			return;
		}
		// the length of the incoming message.
		int messageLength = in.readInt();
		if (in.readableBytes() >= messageLength) {
			// read the encrypted payload data.
			ByteBuf encryptedPayload = in.readBytes(messageLength);
			// write the data onto this byte array.
			byte[] encrypted = new byte[messageLength];
			encryptedPayload.readBytes(encrypted);

			// grab this channels AES key and decrypt the payload.
			Key key = ctx.channel().attr(NetworkConstants.AES_KEY).get();
			byte[] decrypted = AESEncryption.decrypt(encrypted, key);
			// transfer the data onto a decrypted bytebuffer so the remaining
			// message can be read.
			ByteBuf decryptedPayload = Unpooled.wrappedBuffer(decrypted);

			// read the information from the decrypted buffer.
			String username = Utility.readString(decryptedPayload);
			String password = Utility.readString(decryptedPayload);

			// finally remove the login message decoder from the pipeline.
			ctx.pipeline().remove(this);

			// check if the server's player capacity isn't full.
			int players = serverManager.getWorld().getPlayers().size();
			if (players == Settings.MAX_PLAYERS) {
				sendResponse(ctx, new LoginMessageState(SERVER_FULL));
				return;
			}
			// check if the name of the player is already in use.
			if (serverManager.getWorld().playerOnline(username)) {
				sendResponse(ctx, new LoginMessageState(ALREADY_ONLINE));
				return;
			}
			// at this point the request is successful.
			LoginMessageState state = new LoginMessageState(SUCCESS);
			GameSession session = new GameSession(ctx.channel(), serverManager);
			// add a new game session to the pipeline.
			ctx.pipeline().addLast("session", session);

			// create temporary session details.
			PlayerDetails details = new PlayerDetails(username, password);
			EntityPosition defaultPos = new EntityPosition(0, 0);
			EntityDirection defaultDir = EntityDirection.NORTH;
			Player player = new Player(serverManager.getWorld(), session, details, defaultPos, defaultDir);

			player.setName(username);
			player.setNewPlayer(true);
			session.setPlayer(player);

			// add the player to the world.
			serverManager.getWorld().addPlayer(player);

			// respond to the client's login request.
			state.setId(player.getId());
			sendResponse(ctx, state);

		}
	}

	/**
	 * Sends a login response.
	 * 
	 * @param ctx
	 * @param state
	 */
	public void sendResponse(ChannelHandlerContext ctx, LoginMessageState state) {
		switch (state.getState()) {
		case 1:
			ctx.write(state);
			break;
		case 2:
			ctx.write(state).addListener(ChannelFutureListener.CLOSE);
			break;
		default:
			break;
		}
	}
}
