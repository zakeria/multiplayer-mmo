package org.mobmmo.net.codec;

import java.security.Key;
import java.util.List;

import org.mobmmo.ServerManager;
import org.mobmmo.net.NetworkConstants;
import org.mobmmo.net.packet.Packet;
import org.mobmmo.net.security.AESEncryption;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

/**
 * A class that handles the encoding of {@link Packet} messages.
 * 
 * @author Zakeria
 *
 */
public final class PacketMessageEncoder extends MessageToMessageEncoder<Packet> {
	/**
	 * An instance of {@link ServerManager}.
	 */
	private final ServerManager serverManager;

	public PacketMessageEncoder(ServerManager serverManager) {
		this.serverManager = serverManager;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, Packet packet, List<Object> out) throws Exception {
		// The AES encryption key belonging to the client's channel.
		Key key = ctx.channel().attr(NetworkConstants.AES_KEY).get();
		// the data stored in the outgoing
		byte[] data = packet.toByteArray();
		// encrypt the data using the AES algorithm.
		byte[] encryptedData = AESEncryption.encrypt(data, key);
		// initialise a new unpooled buffer with the length of the encrypted data
		ByteBuf buffer = Unpooled.buffer(encryptedData.length + 2);
		// insert the packet values (format of id followed by size and payload.
		buffer.writeByte(packet.getId());		
		buffer.writeInt(encryptedData.length);
		buffer.writeBytes(encryptedData);		
		// send the packet.
		ctx.writeAndFlush(buffer);
	}
}
