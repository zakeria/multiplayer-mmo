package org.mobmmo.net.codec;

/**
 * Represents the state of a login message.
 * 
 * @author Zakeria
 *
 */
public final class LoginMessageState {
	/**
	 * The state of this login request.
	 */
	private final int state;
	/**
	 * The id for this login. Status codes found in {@link LoginMessageDecoder}.
	 */
	private int id;

	public LoginMessageState(int response) {
		this.state = response;
	}

	/**
	 * Get the state
	 * 
	 * @return the state
	 */
	public int getState() {
		return state;
	}

	/**
	 * Get the session id
	 * 
	 * @return the id
	 */
	public int getSessionId() {
		return id;
	}

	/**
	 * Set the session id
	 */
	public void setId(int id) {
		this.id = id;
	}
}
