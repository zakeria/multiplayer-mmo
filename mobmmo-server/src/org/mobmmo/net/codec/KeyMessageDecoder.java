package org.mobmmo.net.codec;

import java.security.Key;
import java.security.interfaces.RSAPrivateKey;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;

import org.mobmmo.ServerManager;
import org.mobmmo.net.NetworkConstants;
import org.mobmmo.net.security.RSAEncryption;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

/**
 * A class that handles the initial exchange between the client and server,
 * where the game client sends the encrypted generated AES key.
 * 
 * @author Zakeria
 *
 */
public final class KeyMessageDecoder extends ByteToMessageDecoder {

	private final ServerManager serverManager;

	public KeyMessageDecoder(ServerManager serverManager) {
		this.serverManager = serverManager;
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		// the size of the encrypted byte array.
		int messageLength = in.readUnsignedByte();
		if (in.readableBytes() >= messageLength) {
			byte[] encryptedKey = new byte[messageLength];
			ByteBuf payload = in.readBytes(encryptedKey);

			System.err.println("excanged" + messageLength + " " + encryptedKey.length);

			RSAPrivateKey privKey = serverManager.getServer().getSettings().getPrivateKey();
			
			byte[] decryptedKey = RSAEncryption.decrypt(encryptedKey, privKey);	
			
			Key key =  new SecretKeySpec(decryptedKey, "AES");

			//set the key belonging to the channel.
			ctx.channel().attr(NetworkConstants.AES_KEY).set(key);
						
			ctx.pipeline().remove(this);
		}
	}
}
