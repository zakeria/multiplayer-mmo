/**
 * Contains network message encoding and decoding related classes.
 */
/**
 * @author Zakeria
 *
 */
package org.mobmmo.net.codec;