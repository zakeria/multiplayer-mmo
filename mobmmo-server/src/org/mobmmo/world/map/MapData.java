package org.mobmmo.world.map;

/**
 * Stores {@link TileMap} data.
 * 
 * @author Zakeria
 *
 */
public final class MapData {

	/**
	 * The tiles belonging to a {@link TileMap}.
	 */
	private final Tile[][] tiles;


	public MapData(Tile[][] tiles) {
		this.tiles = tiles;
	}

	/**
	 * Gets the tile data.
	 * 
	 * @return the tiles
	 */
	public Tile[][] getTiles() {
		return tiles;
	}
}
