package org.mobmmo.world.map;

/**
 * @author Zakeria
 *
 */
public final class Tile {
	/**
	 * The x index of this tile.
	 */
	private final int x;

	/**
	 * The y index of this tile.
	 */
	private final int y;
	/**
	 * The tile configuration.
	 */
	private final TileConfig config;

	public Tile(int x, int y, TileConfig config) {
		this.x = x;
		this.y = y;
		this.config = config;
	}
	/**
	 * Get the x index.
	 * @return the x
	 */
	public int getX() {
		return x;
	}
	/**
	 * Get the y index.
	 * @return the y
	 */
	public int getY() {
		return y;
	}
	/**
	 * Get the tile configuration.
	 * @return the config
	 */
	public TileConfig getTileConfig() {
		return config;
	}
}
