package org.mobmmo.world.map;

/**
 * @author Zakeria
 *
 */
public final class TileMap {

	/**
	 * The id of a map.
	 */
	private final int id;
	/**
	 * The map width.
	 */
	private final int width;
	/**
	 * The map height.
	 */
	private final int height;

	/**
	 * Data belonging a tile map.
	 */
	private final MapData data;

	public TileMap(int id, int width, int height, MapData data) {
		this.id = id;
		this.width = width;
		this.height = height;
		this.data = data;
	}

	/**
	 * Gets the map id.
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the map width.
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Gets the map height.
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Gets the map data.
	 * @return the data
	 */
	public MapData getData() {
		return data;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof TileMap) {
			TileMap other = (TileMap) o;
			if (other.getId() == id) {
				return true;
			}
		}
		return false;
	}
}
