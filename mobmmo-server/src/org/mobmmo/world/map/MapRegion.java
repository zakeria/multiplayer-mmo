package org.mobmmo.world.map;

import java.util.HashSet;
import java.util.Set;

import org.mobmmo.entity.Player;

/**
 * Represents a {@link TileMap} region. (8x8 chunks).
 * 
 * @author Zakeria
 *
 */
public final class MapRegion {
	/**
	 * The Set of {@link Player}s in this region.
	 */
	private final Set<Player> players = new HashSet<>();
	/**
	 * The position of this region.
	 */
	private final RegionPosition position;

	public MapRegion(RegionPosition position) {
		this.position = position;
	}

	/**
	 * Adds a {@link Player} to this region.
	 * 
	 * @param player
	 *            the player.
	 */
	public synchronized void addPlayer(Player player) {
		players.add(player);
		player.setLastRegion(this);
	}

	/**
	 * Removes a {@link Player} from this region.
	 * 
	 * @param player
	 *            the player.
	 */
	public synchronized void removePlayer(Player player) {
		players.remove(player);
	}

	/**
	 * Gets the position of this region.
	 * 
	 * @return position
	 */
	public RegionPosition getPosition() {
		return position;
	}

	/**
	 * Gets the {@link Player}s in this region.
	 * 
	 * @return the players
	 */
	public Set<Player> getPlayers() {
		return players;
	}

}
