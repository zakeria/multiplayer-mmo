package org.mobmmo.world.map;

/**
 * @author Zakeria
 *
 */
public final class RegionPosition {

	/**
	 * The x position of a region.
	 */
	private final int x;

	/**
	 * The x position of a region.
	 */
	private final int y;

	public RegionPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Gets the x position.
	 * 
	 * @return x
	 */
	public int getX() {
		return x;
	}

	/**
	 * Gets the y position.
	 * 
	 * @return y
	 */
	public int getY() {
		return y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegionPosition other = (RegionPosition) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "x : " + x + " y : " + y;
	}

}
