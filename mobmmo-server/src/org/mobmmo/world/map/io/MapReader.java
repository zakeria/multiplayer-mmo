package org.mobmmo.world.map.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mobmmo.world.map.MapData;
import org.mobmmo.world.map.Tile;
import org.mobmmo.world.map.TileConfig;
import org.mobmmo.world.map.TileMap;

/**
 * @author Zakeria
 *
 */
public final class MapReader {

	/**
	 * The directory path to the maps.
	 */
	private static final String LOCATION = "data/maps/";

	/**
	 * The map file name format.
	 */
	private static final String MAP_FILE_PATTERN = "(\\d+).(\\D+)";

	/**
	 * The logger instance for this class.
	 */
	private static final Logger logger = Logger.getLogger(MapReader.class.getName());
	/**
	 * A map of loaded {@link TileMap}s.
	 */
	private final Map<Integer, TileMap> loadedMaps = new HashMap<>();

	/**
	 * Loads the maps.
	 */
	public void loadMaps() {
		logger.info("Loading maps...");
		
		File file = new File(LOCATION);
		Arrays.stream(file.listFiles()).forEach(t -> examineFile(t));
		
		logger.info("Loaded " + loadedMaps.size() + " map(s).");
	}

	/**
	 * Examines a {@link File}, constructs a {@link TileMap} if its a valid
	 * file.
	 * 
	 * @param file
	 *            the file
	 */
	private void examineFile(File file) {
		Pattern pattern = Pattern.compile(MAP_FILE_PATTERN);
		Matcher m = pattern.matcher(file.getName());

		if (!file.isDirectory()) {
			if (!m.matches())
				throw new IllegalArgumentException(file.getName() + " does not match pattern");

			TileMap map = constructTileMap(file);
			loadedMaps.put(map.getId(), map);
		}
	}

	/**
	 * Constructs a {@link TileMap} from a {@link File}.
	 * 
	 * @param file
	 *            the file
	 * @return the constructed {@link TileMap}.
	 */
	private TileMap constructTileMap(File file) {
		MapData mapData = null;
		int mapId = -1;
		int width = -1;
		int height = -1;
		try {

			FileReader reader = new FileReader(file);
			BufferedReader br = new BufferedReader(reader);

			// read the map id, and width.
			String[] mapInfo = br.readLine().split(":");
			mapId = Integer.parseInt(mapInfo[0]);
			width = Integer.parseInt(mapInfo[1]);
			height = Integer.parseInt(mapInfo[2]);

			// read the tile sets used for a map.
			String[] tileSetInfo = br.readLine().split(":");

			// read the lines containing tile information.
			Tile[][] tiles = new Tile[width][height];
			String line = null;
			while ((line = br.readLine()) != null) {
				String[] inputString = line.split(" ");
				for (int i = 0; i < inputString.length; i++) {
					String[] tileString = inputString[i].split(":");
					int tileX = Integer.parseInt(tileString[0]);
					int tileY = Integer.parseInt(tileString[1]);
					int tilesetId = Integer.parseInt(tileString[2]);
					int textureId = Integer.parseInt(tileString[3]);
					boolean walkable = (Integer.parseInt(tileString[4])) == 1 ? true : false;

					TileConfig tileConfig = new TileConfig(tilesetId, textureId, walkable);
					tiles[tileX][tileY] = new Tile(tileX, tileY, tileConfig);
					mapData = new MapData(tiles);
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new TileMap(mapId, width, height, mapData);
	}

	/**
	 * Gets the {@link Map} of loaded {@link TileMap}s.
	 * 
	 * @return the loadedMaps
	 */
	public Map<Integer, TileMap> getLoadedMaps() {
		return loadedMaps;
	}
}
