package org.mobmmo.world;

import java.util.Iterator;
import java.util.Set;

import org.mobmmo.entity.EntityPosition;
import org.mobmmo.entity.EntityStep;
import org.mobmmo.entity.EntityStepManager;
import org.mobmmo.entity.Player;
import org.mobmmo.net.packet.Packet;
import org.mobmmo.net.packet.PacketData;
import org.mobmmo.net.packet.PacketManager;
import org.mobmmo.world.map.MapRegion;

/**
 * @author Zakeria
 *
 */
public final class PlayerUpdate {
	/**
	 * An instance of the {@link World}.
	 */
	private final World world;

	/**
	 * A message id that tells the client that a new {@link Player} will be
	 * added.
	 */
	private static final int NEW_PLAYER = 0;
	/**
	 * A message id that tells the client that a {@link Player} will be removed.
	 */
	private static final int REMOVE_PLAYER = 1;
	/**
	 * A message id that tells the client that a new{@link MapRegion} is being
	 * loaded.
	 */
	private static final int LOADING_REGION = 2;
	/**
	 * A message id that tells the {@link MapRegion} has changed.
	 */
	private static final int REGION_CHANGED = 3;
	/**
	 * A flag that signals when the next call to updatePlayers is ready.
	 */
	private boolean updateReady;

	public PlayerUpdate(World world) {
		this.world = world;
		this.updateReady = true;
	}

	/**
	 * Updates all the {@link Player}s in the {@link World} according to which
	 * {@link MapRegion} they are in (according to the {@link WorldMap}).
	 */
	public void updatePlayers(Set<Player> players) {
		// the PacketManager will be used to process outgoing Packets.
		PacketManager packetManager = world.getServerManager().getPacketManager();
		// get a latest copy of the World's player set.

		for (Iterator<Player> it = players.iterator(); it.hasNext();) {
			Player player = it.next();

			// packet containing player movement details.
			EntityPosition pos = player.getPosition();

			// the current region the player is on.
			MapRegion currentRegion = world.getWorldMap().getRegion(pos.getX(), pos.getY());

			if (player.isLoadingRegion()) {
				Packet loadingRegion = new Packet(7);
				loadingRegion.putByte(LOADING_REGION);
				PacketData data = new PacketData(player.getSession(), loadingRegion);
				packetManager.addOutgoingPacket(data);
				player.setLoadingRegion(false);
			}

			for (Player regionPlayer : currentRegion.getPlayers()) {
				// send the next step taken by a player.
				EntityStepManager esm = regionPlayer.getStepManager();
				if (esm.getQueuedSteps().size() > 0) {
					Packet walkPacket = new Packet(2);
					writeWalkPacket(walkPacket, regionPlayer);
					PacketData data = new PacketData(player.getSession(), walkPacket);
					packetManager.addOutgoingPacket(data);
				}
				// send a request to remove the current player.
				if (player.isSessionTerminated()) {
					Packet removePlayer = new Packet(7);
					removePlayer.putByte(REMOVE_PLAYER);
					writeRemovePlayerPacket(removePlayer, player);
					PacketData data = new PacketData(regionPlayer.getSession(), removePlayer);
					packetManager.addOutgoingPacket(data);
				}
			}
			// send the new player all the local players in the same region.
			if (player.isNewPlayer()) {
				if (player.getLastRegion() == null) {
					currentRegion.addPlayer(player);
				}
				for (Player local : currentRegion.getPlayers()) {
					if (!local.equals(player)) {
						Packet regionPacket = new Packet(7);
						regionPacket.putByte(NEW_PLAYER);
						writeAddPlayerPacket(regionPacket, local);
						PacketData regionData = new PacketData(player.getSession(), regionPacket);
						packetManager.addOutgoingPacket(regionData);

						// tell players already online to add the player.
						Packet addPlayer = new Packet(7);
						addPlayer.putByte(NEW_PLAYER);
						writeAddPlayerPacket(addPlayer, player);
						PacketData data = new PacketData(local.getSession(), addPlayer);
						packetManager.addOutgoingPacket(data);
					}
				}
			}
			if (player.isNewPlayer()) {
				player.setNewPlayer(false);
			}

			// the player has moved into a new region.
			if (!currentRegion.equals(player.getLastRegion())) {
				// tell the client a new region has been reached.
				Packet regionChange = new Packet(7);
				regionChange.putByte(REGION_CHANGED);
				PacketData regionChangeMessage = new PacketData(player.getSession(), regionChange);
				packetManager.addOutgoingPacket(regionChangeMessage);

				// remove the player from the previous region.
				player.getLastRegion().removePlayer(player);
				// the player is new to the region.
				player.setNewPlayer(true);

				// tell everyone player was removed.
				for (Player local : player.getLastRegion().getPlayers()) {
					Packet removePlayer = new Packet(7);
					removePlayer.putByte(REMOVE_PLAYER);
					writeRemovePlayerPacket(removePlayer, player);
					PacketData data = new PacketData(local.getSession(), removePlayer);
					packetManager.addOutgoingPacket(data);
				}

				// tell every one this player was added.
				for (Player local : currentRegion.getPlayers()) {
					if (local.equals(player)) {
						System.exit(0);
					}
					Packet addPlayer = new Packet(7);
					addPlayer.putByte(NEW_PLAYER);
					writeAddPlayerPacket(addPlayer, player);
					PacketData data = new PacketData(local.getSession(), addPlayer);
					packetManager.addOutgoingPacket(data);
				}
				// add the player to the new region.
				currentRegion.addPlayer(player);
			}
			if (player.isSessionTerminated()) {
				currentRegion.removePlayer(player);
				it.remove();
				world.getPlayers().remove(player);
			}
		}
		// update the steps taken by players.
		for (Player player : players) {
			if (player.getStepManager().getQueuedSteps().size() > 0) {
				player.getStepManager().updateStepManager();
			}
		}
		updateReady = true;
	}

	/**
	 * Construct the add player {@link Packet}.
	 * 
	 * @param packet
	 *            the packet being constructed.
	 * @param player
	 *            the player data.
	 */
	private void writeAddPlayerPacket(Packet packet, Player player) {
		EntityPosition pos = player.getPosition();
		packet.putInt(player.getId());
		packet.putString(player.getName());
		packet.putByte(pos.getX());
		packet.putByte(pos.getY());
	}

	/**
	 * Construct the remove player {@link Packet}.
	 * 
	 * @param packet
	 *            the packet being constructed.
	 * @param player
	 *            the player data.
	 */
	private void writeRemovePlayerPacket(Packet packet, Player player) {
		packet.putInt(player.getId());
	}

	/**
	 * Construct the movement {@link Packet}.
	 * 
	 * @param packet
	 *            the packet being constructed.
	 * @param player
	 *            the player data.
	 */
	private void writeWalkPacket(Packet packet, Player player) {
		EntityStep step = player.getStepManager().getQueuedSteps().peek();
		packet.putInt(player.getId());
		packet.putByte(step.isClearStep());
		packet.putByte(step.getPosition().getX());
		packet.putByte(step.getPosition().getY());
		packet.putByte(step.getDirection().getDirectionId());
		packet.putByte(step.isRunning() ? 60 : 30);
	}

	/**
	 * Sets the update ready flag.
	 * 
	 * @param updateReady
	 */
	public void setUpdateReady(boolean updateReady) {
		this.updateReady = updateReady;
	}

	/**
	 * Gets the update ready flag.
	 * 
	 * @return updateReady - the flag
	 */
	public boolean isUpdateReady() {
		return updateReady;
	}
}
