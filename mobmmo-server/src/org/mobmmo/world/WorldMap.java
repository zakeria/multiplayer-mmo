package org.mobmmo.world;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.mobmmo.world.map.MapConstants;
import org.mobmmo.world.map.MapRegion;
import org.mobmmo.world.map.RegionPosition;
import org.mobmmo.world.map.TileMap;

/**
 * 
 * A representation of the world map.
 * 
 * @author Zakeria
 *
 */
public final class WorldMap {

	/**
	 * The game {@link World}.
	 */
	private final World world;

	/**
	 * A set of regions that represent an 8x8 tile block within the world map.
	 */
	private final Map<RegionPosition, MapRegion> regions = new ConcurrentHashMap<>();

	public WorldMap(World world) {
		this.world = world;
	}

	/**
	 * Adds a {@link MapRegion} to the set.
	 * 
	 * @param region
	 *            the region
	 */
	public synchronized void addRegion(MapRegion region) {
		if (!regions.containsKey(region.getPosition()))
			regions.put(region.getPosition(), region);
	}

	/**
	 * Gets a {@link MapRegion} by its position.
	 * 
	 * @param x
	 *            position of the region.
	 * @param y
	 *            the y position of the region.
	 * @return the region
	 */
	public synchronized MapRegion getRegion(int x, int y) {
		RegionPosition position = new RegionPosition(x / MapConstants.CHUNK_WIDTH, y / MapConstants.CHUNK_HEIGHT);
		if (regions.containsKey(position)) {
			return regions.get(position);
		}
		MapRegion region = new MapRegion(position);
		addRegion(region);
		return region;
	}

	/**
	 * Gets the {@link MapRegion}s currently stored in the map.
	 * 
	 * @return regions
	 */
	public Map<RegionPosition, MapRegion> getRegion2() {
		return regions;
	}

	/**
	 * Get a loaded {@link TileMap} by its id.
	 * 
	 * @param id
	 *            the tile map id
	 * @return the tile map
	 */
	public synchronized TileMap getMap(int id) {
		return world.getMapReader().getLoadedMaps().get(id);
	}
}
