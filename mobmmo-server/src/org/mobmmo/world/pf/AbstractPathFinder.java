package org.mobmmo.world.pf;

import org.mobmmo.world.WorldMap;
import org.mobmmo.entity.Entity;
import org.mobmmo.entity.EntityPosition;

/**
 * An abstract path finder used to implement various path finding algorithms
 * such as the {@link AstarPathFinder}.
 * 
 * @author Zakeria
 * 
 */
public abstract class AbstractPathFinder {

	/**
	 * Finds the suitable path according to the given algorithm.
	 * 
	 * @param the
	 *            {@link WorldMap}.
	 * 
	 * @param entity
	 *            the {@link Entity}.
	 * @param start
	 *            the starting {@link EntityPosition}.
	 * @param goal
	 *            the goal {@link EntityPosition}.
	 * @return the final {@link Path}.
	 */
	public abstract Path getPath(WorldMap worldMap, Entity entity, EntityPosition goal);

}