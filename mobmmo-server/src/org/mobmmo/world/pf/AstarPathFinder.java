package org.mobmmo.world.pf;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.mobmmo.entity.Entity;
import org.mobmmo.entity.EntityPosition;
import org.mobmmo.world.WorldMap;
import org.mobmmo.world.map.MapData;
import org.mobmmo.world.map.TileMap;

/**
 * A* path finding algorithm.
 * 
 * @author Zakeria
 *
 */
public final class AstarPathFinder extends AbstractPathFinder {

	/**
	 * The set of open {@link TileNode}s.
	 */
	private final Set<TileNode> open = new HashSet<>();

	/**
	 * The current node.
	 */
	private TileNode current;

	/**
	 * The heuristic distance.
	 * 
	 * @param node
	 *            the neighbouring node.
	 * @param target
	 *            the target node.
	 * @return the distance
	 */
	private int distance(TileNode node, TileNode target) {
		int distance = Math.abs(target.getX() - node.getX()) + Math.abs(target.getY() - node.getY());
		return distance;
	}

	/**
	 * Gets the lowest f cost value by sorting the elements in the open list in
	 * a {@link TreeSet}.
	 * 
	 * @return first value
	 */
	private TileNode getLowestFCostNode() {
		SortedSet<TileNode> sortedSet = new TreeSet<>(open);
		return sortedSet.first();
	}

	/**
	 * Examines a neighbouring node.
	 * 
	 * @param node
	 *            the neighbour node.
	 * @param target
	 *            the target node.
	 */
	private void examineNeighbour(TileNode node, TileNode target) {
		if (node.closed || !node.getConfig().isWalkable()) {
			return;
		}
		// the cost of moving to the node being examined.
		int nextMoveCost = current.gCost + distance(current, node);

		if (nextMoveCost < node.gCost || !open.contains(node)) {
			node.gCost = nextMoveCost;
			node.heuristic = distance(node, target);
			node.fCost = node.gCost + node.heuristic;
			node.parent = current;
			if (!open.contains(node)) {
				open.add(node);
			}
		}
	}

	/**
	 * Searches the given space using the astar path finding algorithim.
	 */
	@Override
	public Path getPath(WorldMap worldMap, Entity entity, EntityPosition goal) {
		// create the new empty path.
		Path path = new Path();
		// test
		TileMap map = worldMap.getMap(0);

		// ignore tiles outside of this map's boundary
		if (goal.getX() < 0 || goal.getY() < 0 || goal.getX() > map.getWidth() || goal.getY() > map.getHeight()) {
			return path;
		}
		// the starting position
		EntityPosition startPos = entity.getPosition();

		// the search space
		TileNode[][] nodes = new TileNode[map.getWidth()][map.getHeight()];

		// get the map data
		MapData data = map.getData();

		// create the search space.
		for (int x = 0; x < map.getWidth(); x++) {
			for (int y = 0; y < map.getHeight(); y++) {
				// get the configuration stored in the actual map tiles.
				nodes[x][y] = new TileNode(x, y, data.getTiles()[x][y].getTileConfig());
			}
		}

		// the starting node
		TileNode start = nodes[startPos.getX()][startPos.getY()];
		// the target node
		TileNode target = nodes[goal.getX()][goal.getY()];

		open.add(start);

		while (open.size() > 0) {
			// get the node with current lowest g+h value.
			current = getLowestFCostNode();

			if (current == target) {
				break;
			}

			open.remove(current);
			current.closed = true;

			int x = current.getX();
			int y = current.getY();

			// neighbouring north
			if (y < map.getHeight() - 1) {
				TileNode examining = nodes[x][y + 1];
				examineNeighbour(examining, target);

			}
			// neighbouring south
			if (y > 0) {
				TileNode examining = nodes[x][y - 1];
				examineNeighbour(examining, target);
			}
			// neighbouring east
			if (x < map.getWidth() - 1) {
				TileNode examining = nodes[x + 1][y];
				examineNeighbour(examining, target);
			}

			// neighbouring west
			if (x > 0) {
				TileNode examining = nodes[x - 1][y];
				examineNeighbour(examining, target);
			}

			// neighbouring north west
			if ((y < map.getHeight() - 1) && (x > 0)) {
				TileNode examining = nodes[x - 1][y + 1];
				examineNeighbour(examining, target);
			}

			// neighbouring north east
			if ((x < map.getWidth() - 1) && (y < map.getHeight() - 1)) {
				TileNode examining = nodes[x + 1][y + 1];
				examineNeighbour(examining, target);
			}

			// neighbouring south west
			if ((y > 0) && (x > 0)) {
				TileNode examining = nodes[x - 1][y - 1];
				examineNeighbour(examining, target);
			}

			// neighbouring south east
			if ((x < map.getWidth() - 1) && (y > 0)) {
				TileNode examining = nodes[x + 1][y - 1];
				examineNeighbour(examining, target);
			}
		}
		// back track the positions to get the final path.
		// add the steps to the Player's path.
		while (current != start) {
			EntityPosition stepPosition = new EntityPosition(current.getX(), current.getY());
			path.add(stepPosition);
			current = current.parent;
		}
		return path;
	}
}