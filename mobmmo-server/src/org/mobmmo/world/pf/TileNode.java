package org.mobmmo.world.pf;

import java.util.TreeSet;

import org.mobmmo.world.map.Tile;
import org.mobmmo.world.map.TileConfig;

/**
 * @author Zakeria
 *
 */
public final class TileNode implements Comparable<TileNode> {

	/**
	 * The {@link Tile} x this node is situated on.
	 */
	private final int x;
	/**
	 * The {@link Tile} y this node is situated on.
	 */
	private final int y;

	/**
	 * The configuration for this tile node.
	 */
	private final TileConfig config;
	/**
	 * The parent of this node.
	 */
	protected TileNode parent;

	/**
	 * A flag indicating whether this node is open.
	 */
	protected boolean closed;
	/**
	 * The g cost of this node.
	 */
	protected int gCost;
	/**
	 * The heuristic of this node to the target node.
	 */
	protected int heuristic;
	/**
	 * The f cost of this node.
	 */
	protected int fCost;

	public TileNode(int x, int y, TileConfig config) {
		this.x = x;
		this.y = y;
		this.config = config;
	}

	/**
	 * Gets the x position.
	 * 
	 * @return x
	 */
	public int getX() {
		return x;
	}

	/**
	 * Gets the y position.
	 * 
	 * @return y
	 */
	public int getY() {
		return y;
	}

	/**
	 * Gets the {@link TileConfig} of the {@link Tile} this node is situated on.
	 * 
	 * @return config
	 */
	public TileConfig getConfig() {
		return config;
	}

	/**
	 * Gets the flag denoting whether this node is open or not.
	 * 
	 * @return closed - the flag
	 */
	public boolean isClosed() {
		return closed;
	}

	/**
	 * Used when sorting the open set with a {@link TreeSet}.
	 */
	@Override
	public int compareTo(TileNode other) {
		if (fCost < other.fCost) {
			return -1;
		} else if (fCost == other.fCost) {
			return 0;
		}
		return 1;
	}

	@Override
	public boolean equals(Object obj) {
		TileNode other = (TileNode) obj;
		if (other.getX() == x && other.y == y) {
			return true;
		}
		return false;
	}

}