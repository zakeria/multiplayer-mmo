package org.mobmmo.world;

import java.util.HashSet;
import java.util.Set;

import org.mobmmo.ServerManager;
import org.mobmmo.entity.NPC;
import org.mobmmo.entity.Player;
import org.mobmmo.net.GameSession;
import org.mobmmo.world.map.io.MapReader;

/**
 * Represents the game World.
 * 
 * @author Zakeria
 *
 */
public final class World {
	/**
	 * The last used client id.
	 */
	private int id;
	/**
	 * The game engine.
	 */
	private final ServerManager serverManager;

	/**
	 * Set of active sessions in this world.
	 */
	private final Set<Player> players = new HashSet<>();

	/**
	 * Set of active npcs in this world.
	 */
	private final Set<NPC> npcs = new HashSet<>();

	/**
	 * The {@link WorldMap}.
	 */
	private final WorldMap worldMap = new WorldMap(this);

	/**
	 * An instance of {@link PlayerUpdate}.
	 */
	private final PlayerUpdate playerUpdate;
	/**
	 * An instance of the {@link MapReader}.
	 */
	private final MapReader mapReader = new MapReader();

	public World(ServerManager serverManager) {
		this.serverManager = serverManager;
		this.playerUpdate = new PlayerUpdate(this);
	}

	/**
	 * Gets the next available id;
	 * 
	 * @return the id
	 */
	private int getNextId() {
		int i = id++;
		if (idExists(id))
			return getNextId();
		return i;
	}

	/**
	 * Checks if an id is already in use.
	 * 
	 * @param id
	 * @return
	 */
	private boolean idExists(int id) {
		for (Player p : players) {
			if (p.getId() == id)
				return true;
		}
		return false;
	}

	/**
	 * Loads the game world.
	 */
	public void loadWorld() {
		mapReader.loadMaps();
	}

	/**
	 * Adds a new {@link GameSession} into this world.
	 * 
	 * @param player
	 */
	public synchronized void addPlayer(Player player) {
		if (players.contains(player))
			return;
		player.setId(getNextId());
		players.add(player);
	}

	/**
	 * Adds a new {@link NPC} into this world.
	 * 
	 * @param npc
	 */
	public synchronized void addNPC(NPC npc) {
		if (npcs.contains(npc))
			return;
		npcs.add(npc);
	}

	/**
	 * Removes an {@link Player}.
	 * 
	 * @param player
	 */
	public synchronized void removePlayer(Player player) {
		players.remove(player);
	}

	/**
	 * Removes an {@link NPC}.
	 * 
	 * @param Npc
	 */
	public synchronized void removeNPC(NPC npc) {
		npcs.remove(npc);
	}

	/**
	 * Updates the game world.
	 * 
	 */
	public void updateGameWorld() {
		if(playerUpdate.isUpdateReady()) {
			Set<Player> players = new HashSet<>(this.players);
			playerUpdate.setUpdateReady(false);
			playerUpdate.updatePlayers(players);
		}
	}

	/**
	 * Checks if a {@link Player} is online.
	 * 
	 * @param name
	 *            the name of the {@link Player}.
	 * @return the online status flag
	 */
	public boolean playerOnline(String name) {
		for (Player p : players) {
			if (p.getName().equals(name))
				return true;
		}
		return false;
	}

	/**
	 * Get the {@link Set} of {@link Player}s currently online.
	 * 
	 * @return players
	 */
	public Set<Player> getPlayers() {
		return players;
	}

	/**
	 * Get the {@link Set} of {@link NPC}s currently on the game server.
	 * 
	 * @return npcs
	 */
	public Set<NPC> getNpcs() {
		return npcs;
	}

	/**
	 * Get the {@link WorldMap}.
	 * 
	 * @return worldMap
	 */
	public WorldMap getWorldMap() {
		return worldMap;
	}

	/**
	 * Get the {@link ServerManager}.
	 * 
	 * @return serverManager
	 */
	public ServerManager getServerManager() {
		return serverManager;
	}

	/**
	 * Get the {@link MapReader}.
	 * 
	 * @return mapReader
	 */
	public MapReader getMapReader() {
		return mapReader;
	}
}
