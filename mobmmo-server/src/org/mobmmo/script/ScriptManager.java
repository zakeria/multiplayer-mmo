package org.mobmmo.script;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Zakeria
 *
 */
public final class ScriptManager {

	/**
	 * A map of evaluated {@link Script}s.
	 */
	public final static Map<Integer, Script> scripts = new HashMap<>();

	/**
	 * Adds a new {@link Script} to the map.
	 * 
	 * @param id
	 *            the identifier of the script/
	 * @param script
	 *            the script.
	 */
	public static void addScript(int id, Script script) {
		scripts.put(id, script);
	}

	/**
	 * Gets a {@link Script}.
	 * 
	 * @param id
	 *            the id of the script.
	 * @return the script if present
	 */
	public static Script getScript(int id) {
		return scripts.get(id);
	}

	/**
	 * Gets the stored {@link Script}s.
	 * 
	 * @return scripts - the scripts
	 */
	public static Map<Integer, Script> getScripts() {
		return scripts;
	}
}
