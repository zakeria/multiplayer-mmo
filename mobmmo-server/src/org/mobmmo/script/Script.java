package org.mobmmo.script;

/**
 * Represents a Ruby script.
 * 
 * @author Zakeria
 */
public abstract class Script {

	/**
	 * The id of this script.
	 * 
	 * @return the id
	 */
	public abstract int id();

	/**
	 * The name of this Script.
	 * 
	 * @return the name
	 */
	public abstract String name();

	/**
	 * The author of the Script.
	 * 
	 * @return the author
	 */
	public abstract String author();

	/**
	 * Executes the script.
	 */
	public abstract void execute(Object... args);
}
