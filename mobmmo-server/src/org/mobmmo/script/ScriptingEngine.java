package org.mobmmo.script;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.logging.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.mobmmo.Settings;

/**
 * 
 * @author Zakeria
 *
 */
public final class ScriptingEngine {

	/**
	 * The ScriptEngineManager.
	 */
	private final ScriptEngineManager scriptEngineManager;

	/**
	 * The Ruby Engine.
	 */
	private final ScriptEngine scriptEngine;

	/**
	 * The logger for this class.
	 */
	private final Logger logger = Logger.getLogger(ScriptingEngine.class.getName());

	public ScriptingEngine() {
		scriptEngineManager = new ScriptEngineManager();
		scriptEngine = scriptEngineManager.getEngineByName("jruby");
	}

	/**
	 * Initialises this script engine.
	 */
	public void initialise() {
		logger.info("Loading scripts...");
		loadScripts(Settings.SCRIPT_DIR);
		loadBootstrap();
	}

	/**
	 * Loads the bootstrap file.
	 */
	private void loadBootstrap() {
		File bootstrap = new File(Settings.SCRIPT_DIR + "/bootstrap.rb");
		try {
			scriptEngine.eval(new InputStreamReader(new FileInputStream(bootstrap)));
		} catch (FileNotFoundException e) {
		} catch (ScriptException e) {
		}
	}

	/**
	 * Loads all script files.
	 */
	private void loadScripts(String dir) {
		File file = new File(dir);
		Arrays.stream(file.listFiles()).forEach(t -> examineFile(t));
	}

	/**
	 * Examines a Ruby file,
	 * 
	 * @param file
	 *            the {@link File}.
	 */
	private void examineFile(File file) {
		try {
			if (file.getName().startsWith("bootstrap")) {
				return;
			}
			if (file.getName().endsWith(".rb")) {
				scriptEngine.eval(new InputStreamReader(new FileInputStream(file)));
			} else if (file.isDirectory()) {
				loadScripts(file.getAbsolutePath());
			}
		} catch (FileNotFoundException e) {
		} catch (ScriptException e) {
		}
	}
}
