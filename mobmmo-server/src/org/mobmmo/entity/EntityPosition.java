package org.mobmmo.entity;

/**
 * @author Zakeria
 */
public final class EntityPosition {

	private final int x;

	private final int y;

	public EntityPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	@Override
	public boolean equals(Object o) {
		EntityPosition position = (EntityPosition) o;

		if (x != position.x) {
			return false;
		}
		if (y != position.y) {
			return false;
		}

		return true;
	}
	
	
	@Override
	public String toString() {
		return "x: " + x + " y: " + y;
	}
}