package org.mobmmo.entity;

import java.util.ArrayDeque;
import java.util.Deque;

import org.mobmmo.world.World;
import org.mobmmo.world.map.MapRegion;

/**
 * Manages {@link EntityStep}s.
 * 
 * @author Zakeria
 *
 */
public final class EntityStepManager {
	/**
	 * A {@link Deque} containing {@link EntityStep}s.
	 */
	private Deque<EntityStep> steps = new ArrayDeque<>();

	/**
	 * The entity this step manager belongs to.
	 */
	private final Entity entity;
	/**
	 * A flag indicating whether the steps queue should be cleared.
	 */
	private boolean clearRequired;
	/**
	 * An instance of the {@link World}.
	 */
	private final World world;

	public EntityStepManager(World world, Entity entity) {
		this.world = world;
		this.entity = entity;
	}

	/**
	 * Adds a new step to the queue.
	 * 
	 * @param step
	 * @param player
	 */
	public void addStep(EntityPosition nextPosition) {
		EntityDirection nextDir = EntityDirection.nextDirection(entity, nextPosition);
		EntityStep step = null;
		if (clearRequired) {
			step = new EntityStep(nextDir, false, nextPosition, true);
			clearRequired = false;
		} else {
			step = new EntityStep(nextDir, false, nextPosition, false);
		}
		steps.add(step);
	}

	/**
	 * Resets the step queue.
	 */
	public void clear() {
		steps.clear();
	}

	/**
	 * Updates the {@link EntityStep}s in this EntityStepManager.
	 */
	public void updateStepManager() {
		if (!clearRequired) {
			if (entity instanceof Player) {
				Player player = (Player) entity;
				// peek the current step from the ArrayDeque.
				EntityStep step = steps.peek();
				// get the position of the current step.
				EntityPosition stepPos = step.getPosition();
				// the MapRegion the step is in.
				MapRegion stepRegion = world.getWorldMap().getRegion(stepPos.getX(), stepPos.getY());

				// were moving into a new region.
				if (!player.getLastRegion().equals(stepRegion)) {
					if (!player.getClientPosition().equals(stepPos) && !player.getName().startsWith("bot")) {
						// wait for the client to load the region before moving
						// on.
						return;
					}
					//the client will start loading the new area.
					player.setLoadingRegion(true);
				}
				// set the new direction of the entity,
				entity.setDirection(step.getDirection());
				// set the new position of the entity.
				entity.setPosition(step.getPosition());
				// update the queue.
				steps.poll();
			}
		}
	}

	/**
	 * Gets the steps in the deque.
	 * 
	 * @return the steps
	 */
	public Deque<EntityStep> getQueuedSteps() {
		return steps;
	}

	/**
	 * Sets the queued steps.
	 * 
	 * @param steps
	 *            the queued steps.
	 */
	public void setQueuedSteps(Deque<EntityStep> queuedSteps) {
		this.steps = queuedSteps;
	}
	/**
	 * Gets the clearRequired flag.
	 * @return clearRequired
	 */
	public boolean isClearRequired() {
		return clearRequired;
	}
	/**
	 * Sets the clearRequired flag.
	 * @param clearRequired
	 */
	public void setClearRequired(boolean clearRequired) {
		this.clearRequired = clearRequired;
	}
}
