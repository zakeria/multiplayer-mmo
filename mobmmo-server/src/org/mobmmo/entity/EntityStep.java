package org.mobmmo.entity;

/**
 * Represents a step taken by an {@link Entity}.
 * 
 * @author Zakeria
 *
 */
public final class EntityStep {
	/**
	 * Determine whether this is a running step.
	 */
	private final boolean running;

	/**
	 * The direction of this step.
	 */
	private final EntityDirection direction;
	/**
	 * The {@link EntityPosition} of this step.
	 */
	private final EntityPosition position;

	private final boolean clearStep;

	public EntityStep(EntityDirection direction, boolean running, EntityPosition position, boolean clearStep) {
		this.direction = direction;
		this.running = running;
		this.position = position;
		this.clearStep = clearStep;
	}

	public EntityDirection getDirection() {
		return direction;
	}

	/**
	 * Get the running flag of this step.
	 * 
	 * @return running
	 */
	public boolean isRunning() {
		return running;
	}

	public int isClearStep() {
		return clearStep == true? 1:0;
	}

	/**
	 * Get the position of this step.
	 * 
	 * @return the position
	 */
	public EntityPosition getPosition() {
		return position;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof EntityStep) {
			EntityStep step = (EntityStep) o;
			if (getPosition().equals(step.getPosition())) {
				return true;
			}
		}
		return false;
	}
}
