package org.mobmmo.entity;

import org.mobmmo.net.GameSession;
import org.mobmmo.world.World;
import org.mobmmo.world.map.MapRegion;

/**
 * @author Zakeria
 * 
 */
public class Player extends Entity {
	/**
	 * The {@link GameSession} belonging to this player.
	 */
	private final GameSession session;
	/**
	 * The last region this player was on.
	 */
	private MapRegion lastRegion;
	/**
	 * Indicates whether a player is new to a {@link MapRegion}.
	 */
	private boolean newPlayer;
	/**
	 * Indicates whether this player is still connected.
	 */
	private boolean sessionTerminated;
	/**
	 * Indicates whether a player is loading a new {@link MapRegion}.
	 */
	private boolean loadingRegion;
	/**
	 * The last {@link EntityPosition} the client sent.
	 */
	private EntityPosition clientPosition;
	/**
	 * This player's details.
	 */
	private final PlayerDetails details;

	public Player(World world, GameSession session, PlayerDetails details, EntityPosition position, EntityDirection direction) {
		super(world, position, direction);
		this.session = session;
		this.details = details;
		this.clientPosition = position;
	}

	/**
	 * Gets the {@link GameSession}.
	 * 
	 * @return session
	 */
	public GameSession getSession() {
		return session;
	}

	/**
	 * Gets the newPlayer flag.
	 * 
	 * @return newPlayer
	 */
	public boolean isNewPlayer() {
		return newPlayer;
	}

	/**
	 * Sets the newPlayer flag.
	 * 
	 * @param newPlayer
	 *            the flag
	 */
	public void setNewPlayer(boolean newPlayer) {
		this.newPlayer = newPlayer;
	}

	/**
	 * Get the last region this player was on.
	 * 
	 * @return lastRegion
	 */
	public MapRegion getLastRegion() {
		return lastRegion;
	}

	/**
	 * Get the last {@link EntityPosition} the client sent.
	 * 
	 * @return clientPosition
	 */
	public EntityPosition getClientPosition() {
		return clientPosition;
	}

	/**
	 * Set the last known client {@link EntityPosition}.
	 * 
	 * @param clientPosition
	 */
	public void setClientPosition(EntityPosition clientPosition) {
		this.clientPosition = clientPosition;
	}

	/**
	 * Set the last {@link MapRegion} the player was on.
	 * 
	 * @param lastRegion
	 */
	public void setLastRegion(MapRegion lastRegion) {
		this.lastRegion = lastRegion;
	}

	/**
	 * Gets the session terminated flag.
	 * 
	 * @return sessionTerminated
	 */
	public boolean isSessionTerminated() {
		return sessionTerminated;
	}

	/**
	 * Sets the session terminated flag.
	 * 
	 * @param sessionTerminated
	 *            the flag
	 */
	public void setSessionTerminated(boolean sessionTerminated) {
		this.sessionTerminated = sessionTerminated;
	}

	/**
	 * Gets the is loading flag.
	 * 
	 * @return loadingRegion
	 */
	public boolean isLoadingRegion() {
		return loadingRegion;
	}

	/**
	 * Sets the is loading flag.
	 * 
	 * @param loadingRegion
	 *            the flag
	 */
	public void setLoadingRegion(boolean loadingRegion) {
		this.loadingRegion = loadingRegion;
	}

	/**
	 * Get this player's details.
	 * 
	 * @return details
	 */
	public PlayerDetails getDetails() {
		return details;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Player) {
			Player player = (Player) other;
			if (id == player.getId()) {
				return true;
			}
		}
		return false;
	}
}
