package org.mobmmo.entity;

import org.mobmmo.world.World;

/**
 * Represents a non-playable character.
 * 
 * @author Zakeria
 *
 */
public final class NPC extends Entity {

	public NPC(World world, EntityPosition position, EntityDirection direction) {
		super(world, position, direction);
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof NPC) {
			NPC npc = (NPC) other;
			if(id == npc.getId()) {
				return true;
			}
		}
		return false;
	}
}
