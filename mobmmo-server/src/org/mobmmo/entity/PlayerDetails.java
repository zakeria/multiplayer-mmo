package org.mobmmo.entity;

/**
 * 
 * @author Zakeria
 */
public final class PlayerDetails {

	/**
	 * The {@link Player}s username.
	 */
	private final String username;
	/**
	 * The {@link Player}s password.
	 */
	private final String password;

	public PlayerDetails(String username, String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * Get the username.
	 * 
	 * @return username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Get the password.
	 * 
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
}
