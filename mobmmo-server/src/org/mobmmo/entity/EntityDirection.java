package org.mobmmo.entity;

/**
 * A class which holds the direction an @link {@link Entity} is heading/facing.
 * 
 * @author Zakeria
 *
 */

public enum EntityDirection {

	NORTH(0, 1, 0), 
	EAST(1, 0, 1), 
	SOUTH(0, -1, 2),
	WEST(-1, 0, 3), 
	SOUTH_EAST(1, -1, 4), 
	SOUTH_WEST(-1, -1,5),
	NORTH_EAST(1,1,6), 
	NORTH_WEST(-1, 1,7); 

	private final int xPlane;
	private final int yPlane;
	private final int directionId;

	private EntityDirection(int xPlane, int yPlane, int directionId) {
		this.xPlane = xPlane;
		this.yPlane = yPlane;
		this.directionId = directionId;
	}

	public int getXPlane() {
		return xPlane;
	}
	
	public int getYPlane() {
		return yPlane;
	}
	public int getDirectionId() {
		return directionId;
	}
	
	/**
	 * Determine the next direction an {@link Entity} will head in.
	 * 
	 * @param current
	 * @param next
	 * @return
	 */
	public static EntityDirection nextDirection(Entity entity, EntityPosition next) {
		EntityPosition last = entity.getPosition();
		if (last.getX() < next.getX()) {
			if (last.getY() == next.getY()) {
				return EntityDirection.EAST;
			} else if (last.getY() < next.getY())
				return EntityDirection.NORTH_EAST;
			else
				return EntityDirection.SOUTH_EAST;
		} else if (last.getX() == next.getX()) {
			if (last.getY() < next.getY())
				return EntityDirection.NORTH;
			else
				return EntityDirection.SOUTH;
		} else if (last.getX() > next.getX()) {
			if (last.getY() == next.getY())
				return EntityDirection.WEST;
			else if (last.getY() < next.getY())
				return EntityDirection.NORTH_WEST;
			else
				return EntityDirection.SOUTH_WEST;
		}
		return entity.getDirection();
	}
	
	public EntityDirection getDirectionByValue(EntityDirection current,  int xPlane, int yPlane){
		for(EntityDirection direction : values()){
			if(xPlane == direction.getXPlane() && yPlane == direction.getYPlane()){
				return direction;
			}
		}
		return current;
	}
}
