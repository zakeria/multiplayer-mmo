package org.mobmmo.entity;

import java.util.HashSet;
import java.util.Set;

import org.mobmmo.net.GameSession;
import org.mobmmo.world.World;

/**
 * @author Zakeria
 * 
 */
public abstract class Entity {

	/**
	 * The position of this entity.
	 */
	private EntityPosition position;
	/**
	 * The {@link EntityDirection} this entity is facing/heading.
	 */
	private EntityDirection direction;

	/**
	 * The id of this entity.
	 */
	protected int id;

	/**
	 * Set of local players.
	 */
	protected final Set<GameSession> localSessions = new HashSet<GameSession>();
	/**
	 * Set of local NPCs.
	 */
	protected final Set<NPC> localNPCs = new HashSet<NPC>();

	/**
	 * The name of the Player. move to wrapper class.
	 */
	protected String name;

	/**
	 * The {@linkplain EntityStepManager} belonging to this entity.
	 */
	protected final EntityStepManager stepManager;;

	public Entity(World world, EntityPosition position, EntityDirection direction) {
		this.stepManager = new EntityStepManager(world, this);
		this.position = position;
		this.direction = direction;
	}

	/**
	 * Sets the position of this entity.
	 * 
	 * @param position
	 *            the position
	 */
	public void setPosition(EntityPosition position) {
		if (position.equals(this.position)) {
			System.out.println("not changed");
			return;
		}
		this.position = position;
	}

	/**
	 * Adds a local player.
	 * 
	 * @param player
	 *            the local player
	 */
	public synchronized void addLocalSession(GameSession player) {
		if (localSessions.contains(player))
			return;
		localSessions.add(player);
	}

	/**
	 * Gets local {@link NPC}s.
	 * 
	 * @return the local npcs
	 */
	public Set<NPC> getLocalNPCs() {
		return localNPCs;
	}

	/**
	 * Gets local {@link Player}s.
	 * 
	 * @return the local players
	 */
	public Set<GameSession> getLocalSessions() {
		return localSessions;
	}

	/**
	 * Get the step manager for this entity.
	 * 
	 * @return the step manager
	 */
	public EntityStepManager getStepManager() {
		return stepManager;
	}

	/**
	 * Gets the position of this entity.
	 */
	public EntityPosition getPosition() {
		return position;
	}
	/**
	 * Get the direction this entity is heading/facing·
	 * @return direction.
	 */
	public EntityDirection getDirection() {
		return direction;
	}
	/**
	 * Sets the direction of this step.
	 * @param direction the {@link EntityDirection}.
	 */
	public void setDirection(EntityDirection direction) {
		this.direction = direction;
	}

	/**
	 * Gets the name of this entity.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of this entity.
	 * 
	 * @param name
	 *            the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the id of this entity.
	 * 
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id of this entity.
	 * 
	 * @param id
	 *            the player id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Implement equals for each instance of entity.
	 */
	public abstract boolean equals(Object o);
}
