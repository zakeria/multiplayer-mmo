A tile-based cross-platform multiplayer game framework written in Java using non-blocking I/O Networking with netty.io. libGDX is used for the game engine.
